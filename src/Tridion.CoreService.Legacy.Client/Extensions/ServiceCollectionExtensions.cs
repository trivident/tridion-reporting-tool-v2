﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Tridion.CoreService.Legacy.Client.Extensions;

public static class ServiceCollectionExtensions
{
    public static void UseLegacyCoreServiceClient(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddHttpClient<ICoreServiceClient, CoreServiceClient>()
            .ConfigureHttpClient((httpClient) =>
            {
                httpClient.BaseAddress = new Uri(configuration["LegacyApi:BaseUrl"]);
            });
    }
}

