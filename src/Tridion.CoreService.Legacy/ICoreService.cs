﻿using System.Collections.Generic;
using Tridion.CoreService.Legacy.Models;

namespace Tridion.CoreService.Legacy
{
    public interface ICoreService
    {
        /// <summary>
        /// Trigger the event system to resubscribe by making a change to a secret item (the GIF multimedia type)
        /// </summary>
        /// <param name="id">tcm:0-1-65544</param>
        bool Resubscribe(string id = "tcm:0-1-65544");

        /// <summary>
        /// Get all available user groups
        /// </summary>
        IReadOnlyCollection<UserGroup> GetUserGroups();

        /// <summary>
        /// Get a specific user
        /// </summary>
        UserResponse GetUser(UserRequest user);
    }
}
