﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.Xml;
using Tridion.ContentManager.CoreService.Client;
using Tridion.CoreService.Legacy.Models;
using Tridion.CoreService.Legacy.Options;

namespace Tridion.CoreService.Legacy
{
    public class CoreService : ICoreService
    {
        private readonly TridionOptions _tridionOptions;
        private readonly ILogger<CoreService> _logger;

        public CoreService(TridionOptions tridionOptions, ILogger<CoreService> logger)
        {
            _tridionOptions = tridionOptions;
            _logger = logger;
        }

        public IReadOnlyCollection<UserGroup> GetUserGroups()
        {
            try
            {
                var filterData = new GroupsFilterData();
                using (var client = GetClient())
                {
                    var availableUserGroups = client.GetSystemWideList(filterData);
                    return availableUserGroups?.Select(group => new UserGroup() { Id = group.Id, Name = group.Title }).ToList().AsReadOnly();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Something went wrong getting user groups...");
                return Enumerable.Empty<UserGroup>().ToList().AsReadOnly();
            }
        }

        public UserResponse GetUser(UserRequest user)
        {
            try
            {
                using (var client = GetClient(new TridionOptions
                {
                    Username = user.UserName,
                    Password = user.Password,
                    Domain = _tridionOptions.Domain,
                    Hostname = _tridionOptions.Hostname,
                }))
                {
                    var currentUser = client.GetCurrentUser();
                    return new UserResponse
                    {
                        UserName = currentUser.Title,
                        Id = currentUser.Id,
                        Groups = currentUser.GroupMemberships?
                            .Select(m => new UserGroup() { Id = m.Group.IdRef, Name = m.Group.Title })
                            .ToList().AsReadOnly()
                    };
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Something went wrong getting user groups by username {}...", user.UserName);
                return null;
            }
        }

        public bool Resubscribe(string id)
        {
            try
            {
                using (var client = GetClient())
                {
                    var mmtype = client.Read(id, new ReadOptions());
                    mmtype.Title += " ";
                    client.Save(mmtype, null);
                }
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Something went wrong resubscribing...");
                return false;
            }
        }

        private SessionAwareCoreServiceClient GetClient()
        {
            return GetClient(_tridionOptions);
        }

        private static SessionAwareCoreServiceClient GetClient(TridionOptions configuration)
        {
            return GetClient(configuration.Hostname, configuration.Username, configuration.Password, configuration.Domain);
        }

        private static SessionAwareCoreServiceClient GetClient(string hostName, string username, string password, string domain)
        {
            var netTcpBinding = new NetTcpBinding
            {
                MaxReceivedMessageSize = 2147483647,
                ReaderQuotas = new XmlDictionaryReaderQuotas
                {
                    MaxStringContentLength = 2147483647,
                    MaxArrayLength = 2147483647
                }
            };

            var remoteAddress =
                new EndpointAddress($"net.tcp://{hostName}:2660/CoreService/{GetContractVersion()}/netTcp");

            var coreServiceClient = new SessionAwareCoreServiceClient(netTcpBinding, remoteAddress);

            if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
            {
                coreServiceClient.ClientCredentials.Windows.ClientCredential.UserName = username;
                coreServiceClient.ClientCredentials.Windows.ClientCredential.Password = password;
            }

            if (!string.IsNullOrEmpty(domain))
            {
                coreServiceClient.ClientCredentials.Windows.ClientCredential.Domain = domain;
            }

            return coreServiceClient;
        }

        private enum CoreServiceInstance
        {
            Tridion2011 = 2011,
            Tridion2013 = 2013,
            SdlWeb8 = 201501,
            SdlWeb85 = 201603,
            Tridion9 = 201701
        }

        private static string _contractVersion;

        private static string GetContractVersion()
        {
            if (_contractVersion == null)
            {
                var contractAttribute = typeof(Tridion.ContentManager.CoreService.Client.ICoreService).GetCustomAttribute<ServiceContractAttribute>(true);

                _contractVersion = contractAttribute?.Namespace?.Split('/')?.LastOrDefault();
            }

            return _contractVersion;
        }
    }
}