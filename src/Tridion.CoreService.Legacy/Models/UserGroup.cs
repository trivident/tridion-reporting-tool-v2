﻿namespace Tridion.CoreService.Legacy.Models
{
    public class UserGroup
    {
        public string Name { get; set; }
        public string Id { get; set; }
    }
}