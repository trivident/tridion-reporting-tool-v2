﻿using System.Collections.Generic;

namespace Tridion.CoreService.Legacy.Models
{
    public class UserResponse
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public IEnumerable<UserGroup> Groups { get; set; }
    }
}