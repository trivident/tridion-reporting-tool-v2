﻿namespace Tridion.CoreService.Legacy.Options
{
    public class TridionOptions
    {
        public string Hostname { get; set; }
        public string Domain { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}

