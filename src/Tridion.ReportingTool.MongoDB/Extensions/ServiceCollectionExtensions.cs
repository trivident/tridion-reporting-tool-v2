﻿using Microsoft.Extensions.DependencyInjection;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using Tridion.ReportingTool.Models;
using Tridion.ReportingTool.Models.Configuration;
using Tridion.ReportingTool.Models.Contracts;
using Tridion.ReportingTool.MongoDB.Options;
using Tridion.ReportingTool.MongoDB.Services;

namespace Tridion.ReportingTool.MongoDB.Extensions;

public static class ServiceCollectionExtensions
{
    public static void UseMongoDb(this IServiceCollection services, Action<MongoOptions> configureOptions)
    {
        services.Configure(configureOptions);

        // TODO is this the correct way to do it?
        var options = new MongoOptions();
        configureOptions.Invoke(options);

        services.AddSingleton<IMongoClient>(new MongoClient(options.ConnectionString));
        RegisterClassMap();

        services.AddSingleton<IConfigurationService, ConfigurationService>();
        services.AddSingleton<IReportService, ReportService>();
        services.AddSingleton<IEventService, EventService>();
    }

    private static void RegisterClassMap()
    {
        BsonClassMap.RegisterClassMap<Event>(cm =>
        {
            cm.AutoMap();
            cm.GetMemberMap(e => e.SubjectType).SetSerializer(new EnumSerializer<ItemType>(BsonType.String));
            cm.GetMemberMap(e => e.BlueprintStatus).SetSerializer(new EnumSerializer<BlueprintStatus>(BsonType.String));
            cm.SetIgnoreExtraElements(true);
        });

        BsonClassMap.RegisterClassMap<Report>(cm =>
        {
            cm.AutoMap();
            cm.SetIgnoreExtraElements(true);
        });

        BsonClassMap.RegisterClassMap<EventConfiguration>(cm =>
        {
            cm.AutoMap();
            cm.SetIgnoreExtraElements(true);
        });

        BsonClassMap.RegisterClassMap<UserGroupConfiguration>(cm =>
        {
            cm.AutoMap();
            cm.SetIgnoreExtraElements(true);
        });
    }
}
