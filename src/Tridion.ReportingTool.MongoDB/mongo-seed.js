﻿print("Started adding indexes.");

db.createCollection('events');
db.reports.createIndex( { "$**": "text" } )
db.events.createIndex({ EventStartDate: 1 })
db.events.createIndex({ EventStartDate: -1 })
db.events.createIndex({ EventName: 1 })
db.events.createIndex({ EventName: -1 })
db.events.createIndex({ EventFamily: 1 })
db.events.createIndex({ EventFamily: -1 })
db.events.createIndex({ UserName: 1 })
db.events.createIndex({ UserName: -1 })
db.events.createIndex({ SubjectId: 1 })
db.events.createIndex({ SubjectId: -1 })
db.events.createIndex({ SubjectName: 1 })
db.events.createIndex({ SubjectName: -1 })
db.events.createIndex({ PublicationName: 1 })
db.events.createIndex({ PublicationName: -1 })
db.events.createIndex({ PublicationId: 1 })
db.events.createIndex({ PublicationId: -1 })
db.events.createIndex({ ElapsedMiliseconds: 1 })
db.events.createIndex({ ElapsedMiliseconds: -1 })
db.events.createIndex({ ServerName: 1 })
db.events.createIndex({ ServerName: -1 })
db.events.createIndex({ TransactionId: 1 })
db.events.createIndex({ TransactionId: -1 })
db.events.createIndex({ SubjectType: 1 })
db.events.createIndex({ SubjectType: -1 })
db.events.createIndex({ EventPhase: 1 })
db.events.createIndex({ EventPhase: -1 })
db.events.createIndex({ WebDavUrl: 1 })
db.events.createIndex({ WebDavUrl: -1 })
db.events.createIndex({ Version: 1 })
db.events.createIndex({ Version: -1 })
db.events.createIndex({ EventName: -1, TransactionId: -1 })
db.events.createIndex({ EventName: 1, TransactionId: 1, EventStartDate: -1 })

db.events.insert({ "EventName": "Copy", "EventFamily": "Crud", "EventPhase": "TransactionCommitted", "EventStartDate": ISODate("2022-01-23T11:58:53.068Z"), "WebDavUrl": "/webdav/100%20Global%20Content/Building%20Blocks/Content/Hero/Hero%202022.xml", "SubjectId": "tcm:2-142", "SubjectName": "Hero 2022", "SubjectType": "Component", "Version": 2, "BlueprintStatus": "Local", "PublicationName": "100 Global Content", "PublicationId": "tcm:0-2-1", "UserId": "tcm:0-21-65552", "UserName": "TRIVIDENT01\\Rachel", "ServerName": "TRIVIDENT01", "ElapsedMilliseconds": 73, "IsNewItem": false, "CheckInAfterSave": false, "Destination": "Folder tcm:2-16-2", "OriginalSubjectId": "tcm:2-284", "ItemsToDelete": null, "PublishUrl": null, "Purpose": null, "TargetType": null });
db.events.insert({ "EventName": "Copy", "EventFamily": "Crud", "EventPhase": "Initiated", "EventStartDate": ISODate("2022-01-23T11:58:55.121Z"), "WebDavUrl": "/webdav/100%20Global%20Content/Building%20Blocks/Content/Hero/Hero%202022.xml", "SubjectId": "tcm:2-142", "SubjectName": "Hero 2022", "SubjectType": "Component", "Version": 2, "BlueprintStatus": "Local", "PublicationName": "100 Global Content", "PublicationId": "tcm:0-2-1", "UserId": "tcm:0-21-65552", "UserName": "TRIVIDENT01\\Rachel", "ServerName": "TRIVIDENT01", "ElapsedMilliseconds": 0, "IsNewItem": false, "CheckInAfterSave": false, "Destination": "Folder tcm:2-16-2", "OriginalSubjectId": null, "ItemsToDelete": null, "PublishUrl": null, "Purpose": null, "TargetType": null });
db.events.insert({ "EventName": "CheckOut", "EventFamily": "Lock", "EventPhase": "TransactionCommitted", "EventStartDate": ISODate("2022-01-23T11:58:56.455Z"), "WebDavUrl": "/webdav/100%20Global%20Content/Building%20Blocks/Content/Hero/Copy%20of%20Hero%202022.xml", "SubjectId": "tcm:2-284", "SubjectName": "Copy of Hero 2022", "SubjectType": "Component", "Version": 1, "BlueprintStatus": "Local", "PublicationName": "100 Global Content", "PublicationId": "tcm:0-2-1", "UserId": "tcm:0-21-65552", "UserName": "TRIVIDENT01\\Rachel", "ServerName": "TRIVIDENT01", "ElapsedMilliseconds": 32, "LockType": "CheckedOut" });
db.events.insert({ "EventName": "CheckOut", "EventFamily": "Lock", "EventPhase": "Initiated", "EventStartDate": ISODate("2022-01-23T12:00:18.986Z"), "WebDavUrl" : "/webdav/100%20Global%20Content/Building%20Blocks/Content/Hero/Copy%20of%20Hero%202022.xml", "SubjectId" : "tcm:2-284", "SubjectName" : "Copy of Hero 2022", "SubjectType" : "Component", "Version" : 1, "BlueprintStatus" : "Local", "PublicationName" : "100 Global Content", "PublicationId" : "tcm:0-2-1", "UserId" : "tcm:0-21-65552", "UserName" : "TRIVIDENT01\\Rachel", "ServerName" : "TRIVIDENT01", "ElapsedMilliseconds" : 0, "LockType" : "CheckedOut" });
db.events.insert({ "EventName": "CheckIn", "EventFamily": "Unlock", "EventPhase": "TransactionCommitted", "EventStartDate": ISODate("2022-01-23T12:00:19.181Z"), "WebDavUrl": "/webdav/100%20Global%20Content/Building%20Blocks/Content/Hero/Central%20Perk.xml", "SubjectId": "tcm:2-284", "SubjectName": "Central Perk", "SubjectType": "Component", "Version": 2, "BlueprintStatus": "Local", "PublicationName": "100 Global Content", "PublicationId": "tcm:0-2-1", "UserId": "tcm:0-21-65552", "UserName": "TRIVIDENT01\\Rachel", "ServerName": "TRIVIDENT01", "ElapsedMilliseconds": 13, "LockType": null });
db.events.insert({ "EventName": "CheckOut", "EventFamily": "Lock", "EventPhase": "TransactionCommitted", "EventStartDate": ISODate("2022-01-23T12:01:02.781Z"), "WebDavUrl": "/webdav/100%20Global%20Content/Building%20Blocks/Content/Images/Quotes/Quirijn%20Slings.jpg", "SubjectId": "tcm:2-231", "SubjectName": "Quirijn Slings", "SubjectType": "Component", "Version": 2, "BlueprintStatus": "Local", "PublicationName": "100 Global Content", "PublicationId": "tcm:0-2-1", "UserId": "tcm:0-21-65552", "UserName": "TRIVIDENT01\\Rachel", "ServerName": "TRIVIDENT01", "ElapsedMilliseconds": 20, "LockType": "CheckedOut" });
db.events.insert({ "EventName": "UndoCheckOut", "EventFamily": "Unlock", "EventPhase": "TransactionCommitted", "EventStartDate": ISODate("2022-01-23T12:01:06.005Z"), "WebDavUrl": "/webdav/100%20Global%20Content/Building%20Blocks/Content/Images/Quotes/Quirijn%20Slings.jpg", "SubjectId": "tcm:2-231", "SubjectName": "Quirijn Slings", "SubjectType": "Component", "Version": 2, "BlueprintStatus": "Local", "PublicationName": "100 Global Content", "PublicationId": "tcm:0-2-1", "UserId": "tcm:0-21-65552", "UserName": "TRIVIDENT01\\Rachel", "ServerName": "TRIVIDENT01", "ElapsedMilliseconds": 15, "LockType": null });
db.events.insert({ "EventName": "CheckIn", "EventFamily": "Unlock", "EventPhase": "TransactionCommitted", "EventStartDate": ISODate("2022-01-23T12:02:13.559Z"), "WebDavUrl": "/webdav/100%20Global%20Content/Building%20Blocks/Content/Images/Quotes/Friends.jpeg", "SubjectId": "tcm:2-285", "SubjectName": "Friends", "SubjectType": "Component", "Version": 1, "BlueprintStatus": "Local", "PublicationName": "100 Global Content", "PublicationId": "tcm:0-2-1", "UserId": "tcm:0-21-65552", "UserName": "TRIVIDENT01\\Rachel", "ServerName": "TRIVIDENT01", "ElapsedMilliseconds": 14, "LockType": null });
db.events.insert({ "EventName": "Copy", "EventFamily": "Crud", "EventPhase": "TransactionCommitted", "EventStartDate": ISODate("2022-01-23T12:02:21.784Z"), "WebDavUrl": "/webdav/100%20Global%20Content/Building%20Blocks/Content/Images/Quotes/Friends.jpeg", "SubjectId": "tcm:2-285", "SubjectName": "Friends", "SubjectType": "Component", "Version": 1, "BlueprintStatus": "Local", "PublicationName": "100 Global Content", "PublicationId": "tcm:0-2-1", "UserId": "tcm:0-21-65552", "UserName": "TRIVIDENT01\\Rachel", "ServerName": "TRIVIDENT01", "ElapsedMilliseconds": 10, "IsNewItem": false, "CheckInAfterSave": false, "Destination": "Folder tcm:2-35-2", "OriginalSubjectId": "tcm:2-286", "ItemsToDelete": null, "PublishUrl": null, "Purpose": null, "TargetType": null });
db.events.insert({ "EventName": "Copy", "EventFamily": "Crud", "EventPhase": "TransactionCommitted", "EventStartDate": ISODate("2022-01-23T12:02:23.303Z"), "WebDavUrl": "/webdav/100%20Global%20Content/Building%20Blocks/Content/Images/Quotes/Friends.jpeg", "SubjectId": "tcm:2-285", "SubjectName": "Friends", "SubjectType": "Component", "Version": 1, "BlueprintStatus": "Local", "PublicationName": "100 Global Content", "PublicationId": "tcm:0-2-1", "UserId": "tcm:0-21-65552", "UserName": "TRIVIDENT01\\Rachel", "ServerName": "TRIVIDENT01", "ElapsedMilliseconds": 10, "IsNewItem": false, "CheckInAfterSave": false, "Destination": "Folder tcm:2-35-2", "OriginalSubjectId": "tcm:2-287", "ItemsToDelete": null, "PublishUrl": null, "Purpose": null, "TargetType": null });
db.events.insert({ "EventName": "Save", "EventFamily": "Crud", "EventPhase": "Initiated", "EventStartDate": ISODate("2022-01-23T12:04:27.620Z"), "WebDavUrl": "/webdav/300%20Website%20Parent/Home/New%20Structure%20Group", "SubjectId": "tcm:0-0-0", "SubjectName": "Things to do in Manhattan", "SubjectType": "StructureGroup", "Version": 0, "BlueprintStatus": "Local", "PublicationName": " ", "PublicationId": "tcm:0-0-7", "UserId": "tcm:0-18-65552", "UserName": "TRIVIDENT01\\Monica", "ServerName": "TRIVIDENT01", "ElapsedMilliseconds": 0, "IsNewItem": true, "CheckInAfterSave": false, "Destination": null, "OriginalSubjectId": null, "ItemsToDelete": null, "PublishUrl": null, "Purpose": null, "TargetType": null });
db.events.insert({ "EventName": "Delete", "EventFamily": "Crud", "EventPhase": "TransactionCommitted", "EventStartDate": ISODate("2022-01-23T12:03:09.097Z"), "WebDavUrl": "/webdav/100%20Global%20Content/Building%20Blocks/Content/Images/Quotes/Copy%20of%20Friends.jpeg", "SubjectId": "tcm:2-286", "SubjectName": "Copy of Friends", "SubjectType": "Component", "Version": 1, "BlueprintStatus": "Local", "PublicationName": "100 Global Content", "PublicationId": "tcm:0-2-1", "UserId": "tcm:0-21-65552", "UserName": "TRIVIDENT01\\Rachel", "ServerName": "TRIVIDENT01", "ElapsedMilliseconds": 342, "IsNewItem": false, "CheckInAfterSave": false, "Destination": null, "OriginalSubjectId": null, "ItemsToDelete": null, "PublishUrl": null, "Purpose": null, "TargetType": null });
db.events.insert({ "EventName": "Delete", "EventFamily": "Crud", "EventPhase": "TransactionCommitted", "EventStartDate": ISODate("2022-01-23T12:03:09.097Z"), "WebDavUrl": "/webdav/100%20Global%20Content/Building%20Blocks/Content/Images/Quotes/Copy%20of%20Friends.jpeg", "SubjectId": "tcm:2-286", "SubjectName": "Copy of Friends", "SubjectType": "Component", "Version": 1, "BlueprintStatus": "Local", "PublicationName": "100 Global Content", "PublicationId": "tcm:0-2-1", "UserId": "tcm:0-18-65552", "UserName": "TRIVIDENT01\\Monica", "ServerName": "TRIVIDENT01", "ElapsedMilliseconds": 342, "IsNewItem": false, "CheckInAfterSave": false, "Destination": null, "OriginalSubjectId": null, "ItemsToDelete": null, "PublishUrl": null, "Purpose": null, "TargetType": null });


db.createCollection('reports');
db.reports.createIndex( { "$**": "text" } )
db.reports.createIndex({ Date: 1 })
db.reports.createIndex({ Date: -1 })
db.reports.createIndex({ UserId: 1 })
db.reports.createIndex({ UserId: -1 })
db.reports.createIndex({ ReportName: 1 })
db.reports.createIndex({ ReportName: -1 })
db.reports.createIndex({ UserName: 1 })
db.reports.createIndex({ UserName: -1 })

db.reports.insert({ "ReportId": "1", "ReportName": "german-publishing", "CompressedSettings": "eyJ0aW1lIjoxNjQ1MDAyNTI1OTczLCJzdGFydCI6MCwibGVuZ3RoIjoxMCwib3JkZXIiOltbMywiZGVzYyJdXSwic2VhcmNoIjp7InNlYXJjaCI6IiIsInNtYXJ0Ijp0cnVlLCJyZWdleCI6ZmFsc2UsImNhc2VJbnNlbnNpdGl2ZSI6dHJ1ZX0sImNvbHVtbnMiOlt7InZpc2libGUiOnRydWUsInNlYXJjaCI6eyJzZWFyY2giOiIiLCJzbWFydCI6dHJ1ZSwicmVnZXgiOmZhbHNlLCJjYXNlSW5zZW5zaXRpdmUiOnRydWV9fSx7InZpc2libGUiOnRydWUsInNlYXJjaCI6eyJzZWFyY2giOiIiLCJzbWFydCI6dHJ1ZSwicmVnZXgiOmZhbHNlLCJjYXNlSW5zZW5zaXRpdmUiOnRydWV9fSx7InZpc2libGUiOnRydWUsInNlYXJjaCI6eyJzZWFyY2giOiIiLCJzbWFydCI6dHJ1ZSwicmVnZXgiOmZhbHNlLCJjYXNlSW5zZW5zaXRpdmUiOnRydWV9fSx7InZpc2libGUiOnRydWUsInNlYXJjaCI6eyJzZWFyY2giOiIiLCJzbWFydCI6dHJ1ZSwicmVnZXgiOmZhbHNlLCJjYXNlSW5zZW5zaXRpdmUiOnRydWV9fSx7InZpc2libGUiOnRydWUsInNlYXJjaCI6eyJzZWFyY2giOiIiLCJzbWFydCI6dHJ1ZSwicmVnZXgiOmZhbHNlLCJjYXNlSW5zZW5zaXRpdmUiOnRydWV9fSx7InZpc2libGUiOnRydWUsInNlYXJjaCI6eyJzZWFyY2giOiIiLCJzbWFydCI6dHJ1ZSwicmVnZXgiOmZhbHNlLCJjYXNlSW5zZW5zaXRpdmUiOnRydWV9fSx7InZpc2libGUiOnRydWUsInNlYXJjaCI6eyJzZWFyY2giOiIiLCJzbWFydCI6dHJ1ZSwicmVnZXgiOmZhbHNlLCJjYXNlSW5zZW5zaXRpdmUiOnRydWV9fSx7InZpc2libGUiOnRydWUsInNlYXJjaCI6eyJzZWFyY2giOiIiLCJzbWFydCI6dHJ1ZSwicmVnZXgiOmZhbHNlLCJjYXNlSW5zZW5zaXRpdmUiOnRydWV9fSx7InZpc2libGUiOnRydWUsInNlYXJjaCI6eyJzZWFyY2giOiIiLCJzbWFydCI6dHJ1ZSwicmVnZXgiOmZhbHNlLCJjYXNlSW5zZW5zaXRpdmUiOnRydWV9fSx7InZpc2libGUiOnRydWUsInNlYXJjaCI6eyJzZWFyY2giOiIiLCJzbWFydCI6dHJ1ZSwicmVnZXgiOmZhbHNlLCJjYXNlSW5zZW5zaXRpdmUiOnRydWV9fSx7InZpc2libGUiOmZhbHNlLCJzZWFyY2giOnsic2VhcmNoIjoiIiwic21hcnQiOnRydWUsInJlZ2V4IjpmYWxzZSwiY2FzZUluc2Vuc2l0aXZlIjp0cnVlfX0seyJ2aXNpYmxlIjpmYWxzZSwic2VhcmNoIjp7InNlYXJjaCI6IiIsInNtYXJ0Ijp0cnVlLCJyZWdleCI6ZmFsc2UsImNhc2VJbnNlbnNpdGl2ZSI6dHJ1ZX19LHsidmlzaWJsZSI6dHJ1ZSwic2VhcmNoIjp7InNlYXJjaCI6IiIsInNtYXJ0Ijp0cnVlLCJyZWdleCI6ZmFsc2UsImNhc2VJbnNlbnNpdGl2ZSI6dHJ1ZX19LHsidmlzaWJsZSI6ZmFsc2UsInNlYXJjaCI6eyJzZWFyY2giOiIiLCJzbWFydCI6dHJ1ZSwicmVnZXgiOmZhbHNlLCJjYXNlSW5zZW5zaXRpdmUiOnRydWV9fSx7InZpc2libGUiOmZhbHNlLCJzZWFyY2giOnsic2VhcmNoIjoiIiwic21hcnQiOnRydWUsInJlZ2V4IjpmYWxzZSwiY2FzZUluc2Vuc2l0aXZlIjp0cnVlfX0seyJ2aXNpYmxlIjpmYWxzZSwic2VhcmNoIjp7InNlYXJjaCI6IiIsInNtYXJ0Ijp0cnVlLCJyZWdleCI6ZmFsc2UsImNhc2VJbnNlbnNpdGl2ZSI6dHJ1ZX19LHsidmlzaWJsZSI6ZmFsc2UsInNlYXJjaCI6eyJzZWFyY2giOiIiLCJzbWFydCI6dHJ1ZSwicmVnZXgiOmZhbHNlLCJjYXNlSW5zZW5zaXRpdmUiOnRydWV9fV0sImZpbHRlcnMiOlt7ImZpZWxkIjoiRXZlbnROYW1lIiwidmFsdWUiOiJQdWJsaXNoIn0seyJmaWVsZCI6IlB1YmxpY2F0aW9uTmFtZSIsInZhbHVlIjoiNDAwIEdlcm1hbnkgd2Vic2l0ZSAoR2VybWFuKSJ9XSwic3RhcnREYXRlIjoiMjAyMi0wMS0wMSAwMDowMCIsImVuZERhdGUiOiIyMDIyLTAxLTMxIDIzOjU5IiwiZGF0ZVJhbmdlIjoiZGF0ZS1yYWRpby1sYXN0LW1vbnRoIn0=", "Date": ISODate("2022-02-16T09:09:11.160Z"), "UserId": "tcm:0-12-65552", "UserName": "TRIVIDENT01\\Administrator", "Shared": true, "Predefined": false });

db.createCollection('config');
db.config.insert({ "EventName": "CrudEventArgs", "Enabled": true })
db.config.insert({ "EventName": "PublishOrUnPublishEventArgs", "Enabled": true })
db.config.insert({ "EventName": "VersioningEventArgs", "Enabled": true })
db.config.insert({ "EventName": "BlueprintingEventArgs", "Enabled": true })

db.createCollection('usersConfig');
db.usersConfig.insert({ "UserGroupId": "tcm:0-1-65568", "UserGroupName": "Everyone", "Enabled": true })