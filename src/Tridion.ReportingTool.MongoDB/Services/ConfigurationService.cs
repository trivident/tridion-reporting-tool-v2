﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using Tridion.ReportingTool.Models.Configuration;
using Tridion.ReportingTool.Models.Contracts;
using Tridion.ReportingTool.MongoDB.Options;

namespace Tridion.ReportingTool.MongoDB.Services;

public class ConfigurationService : IConfigurationService
{
    private readonly MongoOptions _options;
    private readonly ILogger<ConfigurationService> _logger;
    private readonly IMongoCollection<UserGroupConfiguration> _userGroupConfigurationCollection;
    private readonly IMongoCollection<EventConfiguration> _eventConfigurationCollection;

    public ConfigurationService(IMongoClient mongoClient, IOptions<MongoOptions> options, ILogger<ConfigurationService> logger)
    {
        _logger = logger;
        _options = options.Value;

        var mongoDatabase = mongoClient.GetDatabase(_options.Database);

        _userGroupConfigurationCollection = mongoDatabase.GetCollection<UserGroupConfiguration>("usersConfig");
        _eventConfigurationCollection = mongoDatabase.GetCollection<EventConfiguration>("config");
    }

    public async Task Delete(UserGroupConfiguration usergroup, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("Delete() - {}", usergroup.UserGroupId);
        await _userGroupConfigurationCollection.DeleteOneAsync(x => x.UserGroupId == usergroup.UserGroupId, cancellationToken);
    }

    public async Task EnableEvent(string eventName, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("EnableEvent() - {}", eventName);

        var filter = Builders<EventConfiguration>.Filter.Eq("EventName", eventName);
        var update = Builders<EventConfiguration>.Update.Set("Enabled", true);
        await _eventConfigurationCollection.UpdateOneAsync(filter, update, cancellationToken: cancellationToken);
    }

    public async Task DisableEvent(string eventName, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("DisableEvent() - {}", eventName);

        var filter = Builders<EventConfiguration>.Filter.Eq("EventName", eventName);
        var update = Builders<EventConfiguration>.Update.Set("Enabled", false);
        await _eventConfigurationCollection.UpdateOneAsync(filter, update, cancellationToken: cancellationToken);
    }

    public async Task EnableUserGroup(string userGroupId, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("EnableUserGroup() - {}", userGroupId);
        var filter = Builders<UserGroupConfiguration>.Filter.Eq("UserGroupId", userGroupId);
        var update = Builders<UserGroupConfiguration>.Update.Set("Enabled", true);

        await _userGroupConfigurationCollection.UpdateOneAsync(filter, update, new UpdateOptions() { IsUpsert = true }, cancellationToken: cancellationToken);
    }

    public async Task DisableUserGroup(string userGroupId, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("DisableUserGroup() - {}", userGroupId);

        var filter = Builders<UserGroupConfiguration>.Filter.Eq("UserGroupId", userGroupId);
        var update = Builders<UserGroupConfiguration>.Update.Set("Enabled", false);
        await _userGroupConfigurationCollection.UpdateOneAsync(filter, update, new UpdateOptions() { IsUpsert = true }, cancellationToken: cancellationToken);
    }

    public async Task<IReadOnlyList<EventConfiguration>> GetEventConfiguration(CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("GetEventConfiguration()");
        return await _eventConfigurationCollection.Find(_ => true).ToListAsync(cancellationToken);
    }

    public async Task<IReadOnlyList<UserGroupConfiguration>> GetUserGroupConfiguration(CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("GetUserGroupConfiguration()");
        return await _userGroupConfigurationCollection.Find(_ => true).ToListAsync(cancellationToken);
    }

    public async Task Insert(UserGroupConfiguration usergroup, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("Insert() - {}", usergroup.UserGroupId); 
        await _userGroupConfigurationCollection.InsertOneAsync(usergroup, new InsertOneOptions(), cancellationToken: cancellationToken);
    }
}
