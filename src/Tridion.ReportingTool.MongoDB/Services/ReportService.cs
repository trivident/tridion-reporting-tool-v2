﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Security.Claims;
using Tridion.ReportingTool.Models;
using Tridion.ReportingTool.Models.Authorization;
using Tridion.ReportingTool.Models.Contracts;
using Tridion.ReportingTool.MongoDB.Options;
using DeleteResult = Tridion.ReportingTool.Models.DeleteResult;
using UpdateResult = Tridion.ReportingTool.Models.UpdateResult;

namespace Tridion.ReportingTool.MongoDB.Services;

public class ReportService : IReportService
{
    private readonly MongoOptions _options;
    private readonly ILogger<ReportService> _logger;
    private readonly IMongoCollection<Report> _reportCollection;
    private readonly IAuthorizationService _authService;
    private readonly IHttpContextAccessor _httpContextAccessor;

    public ReportService(IMongoClient mongoClient, IAuthorizationService authService, IHttpContextAccessor httpContextAccessor, IOptions<MongoOptions> options, ILogger<ReportService> logger)
    {
        _logger = logger;
        _options = options.Value;
        _httpContextAccessor = httpContextAccessor;
        _authService = authService;

        var mongoDatabase = mongoClient.GetDatabase(_options.Database);
        _reportCollection = mongoDatabase.GetCollection<Report>("reports");
    }

    public async Task<Report?> Get(Guid id, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("Get() - {}", id);
        var userName = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.Name)?.Value;
        var report = await _reportCollection.Find(x => x.ReportId == id).SingleOrDefaultAsync(cancellationToken);
        var authorizationResult = await _authService.AuthorizeAsync(_httpContextAccessor.HttpContext.User, null, Policies.IsAdmin);

        if (report is not null && (authorizationResult.Succeeded || report.IsPublic || report.UserName == userName))
        {
            return report;
        }
        return null;
    }

    public async Task<DeleteResult> Delete(Guid id, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("Delete() - {}", id);

        var userName = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.Name)?.Value;
        var report = await _reportCollection.Find(x => x.ReportId == id).SingleOrDefaultAsync(cancellationToken);

        if (report is null)
        {
            return new DeleteResult(ResultCode.NotFound);
        }

        var authorizationResult = await _authService.AuthorizeAsync(_httpContextAccessor.HttpContext.User, null, Policies.IsAdmin);
        if (report.UserName == userName || authorizationResult.Succeeded)
        {
            var result = await _reportCollection.DeleteOneAsync(x => x.ReportId == id, cancellationToken);
            return new DeleteResult(result.IsAcknowledged ? ResultCode.Success : ResultCode.Failed);

        }

        return new DeleteResult(ResultCode.UnAuthorized);
    }

    public async Task<InsertResult> Insert(Report report, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("Insert() - {}", report.ReportId);
        await _reportCollection.InsertOneAsync(report, cancellationToken: cancellationToken);
        return new InsertResult(ResultCode.Success);
    }

    public async Task<DeleteResult> RemoveUser(string userName, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("RemoveUser() - {}", userName);

        var authorizationResult = await _authService.AuthorizeAsync(_httpContextAccessor.HttpContext.User, null, Policies.IsAdmin);

        if (authorizationResult.Succeeded)
        {
            var filter = Builders<Report>.Filter.Eq("UserName", userName);
            var update = Builders<Report>.Update
                .Set("UserName", "Anonymous")
                .Set("UserId", "tcm:0-0-0");
            var result = await _reportCollection.UpdateManyAsync(filter, update, cancellationToken: cancellationToken);
            return new DeleteResult(result.IsAcknowledged ? ResultCode.Success : ResultCode.Failed);
        }

        return new DeleteResult(ResultCode.UnAuthorized);
    }

    public async Task<UpdateResult> Update(Report report, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("Update() - {}", report.ReportId);
        var userName = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.Name)?.Value;

        var existingReport = await _reportCollection.Find(x => x.ReportId == report.ReportId).SingleOrDefaultAsync(cancellationToken);

        if (existingReport is null)
        {
            return new UpdateResult(ResultCode.NotFound);
        }

        var authorizationResult = await _authService.AuthorizeAsync(_httpContextAccessor.HttpContext.User, null, Policies.IsAdmin);

        if (existingReport.UserName == userName || authorizationResult.Succeeded)
        {
            var result = await _reportCollection.ReplaceOneAsync(x => x.ReportId == report.ReportId, report, cancellationToken: cancellationToken);
            return new UpdateResult(result.IsAcknowledged ? ResultCode.Success : ResultCode.Failed);
        }

        return new UpdateResult(ResultCode.UnAuthorized);
    }

    public async Task<IReadOnlyCollection<string>> GetSuggestions(string query, string field, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("GetSuggestions - {},{}", query, field);
        var filter = Builders<Report>.Filter.Regex(field, new BsonRegularExpression(query, "i"));
        var result = await _reportCollection.DistinctAsync<string>(field, filter, cancellationToken: cancellationToken);
        return await result.ToListAsync(cancellationToken);
    }


    public async Task<SearchResult<Report>> GetReports(ReportSearchFilter filter, int page, int pageSize, string? sortfield, Models.SortDirection? sortdirection, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("GetReports()");

        var authorizationResult = await _authService.AuthorizeAsync(_httpContextAccessor.HttpContext.User, null, Policies.IsAdmin);

        var filterDefinition = CreateFilterDefinition(filter, isAdmin: authorizationResult.Succeeded);
        var sortDefinition = CreateSortDefinition(sortfield, sortdirection);

        var countFacet = AggregateFacet.Create("count",
             PipelineDefinition<Report, AggregateCountResult>.Create(new[]
             {
                 PipelineStageDefinitionBuilder.Count<Report>()
             })
         );
        var dataFacet = AggregateFacet.Create("data",
            PipelineDefinition<Report, Report>.Create(new[]
            {
                PipelineStageDefinitionBuilder.Sort(sortDefinition),
                PipelineStageDefinitionBuilder.Skip<Report>(page * pageSize),
                PipelineStageDefinitionBuilder.Limit<Report>(pageSize),
            })
        );
        var aggregateResult = await _reportCollection.Aggregate()
            .Match(filterDefinition)
            .Facet(countFacet, dataFacet)
            .SingleAsync(cancellationToken);

        if (!aggregateResult.Facets.Any())
        {
            return new SearchResult<Report>();
        }

        var users = CreateAggregateFacet("$UserName", "usernames");
        var facets = await _reportCollection.Aggregate()
            .Facet(users)
            .SingleAsync(cancellationToken);

        var buckets = facets.Facets
                .Select(facet => new AggregationBucket
                {
                    Name = facet.Name,
                    Results = facet.Output<AggregateSortByCountResult<string>>()
                                    .Select(i => new AggregationResult { Count = i.Count, Key = i.Id })
                                    .ToList()
                }).ToList().AsReadOnly();

        return new SearchResult<Report>
        {
            Items = aggregateResult.Facets.First(x => x.Name == "data").Output<Report>(),
            TotalItems = (int)(aggregateResult.Facets.First(x => x.Name == "count").Output<AggregateCountResult>().FirstOrDefault()?.Count ?? 0),
            Filters = new SearchAggregationResult(buckets)
        };
    }

    private static AggregateFacet<Report, AggregateSortByCountResult<string>> CreateAggregateFacet(string fieldName, string facetName)
    {
        return AggregateFacet.Create(facetName,
            PipelineDefinition<Report, AggregateSortByCountResult<string>>.Create(new[]
            {
                PipelineStageDefinitionBuilder.SortByCount<Report, string>(fieldName)
            })
        );
    }

    private FilterDefinition<Report> CreateFilterDefinition(ReportSearchFilter filter, bool isAdmin)
    {
        if (filter is null)
        {
            return Builders<Report>.Filter.Empty;
        }

        var fieldDefinitions = new List<FilterDefinition<Report>>();

        if (filter.DateRange?.Start != null)
        {
            fieldDefinitions.Add(Builders<Report>.Filter.Gte(t => t.Date, filter.DateRange?.Start));
        }

        if (filter.DateRange?.End != null)
        {
            fieldDefinitions.Add(Builders<Report>.Filter.Lte(t => t.Date, filter.DateRange?.End));
        }

        if (!string.IsNullOrEmpty(filter.Query))
        {
            fieldDefinitions.Add(Builders<Report>.Filter.Text(filter.Query));
        }

        if (!isAdmin)
        {
            var userName = _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.Name)?.Value;
            fieldDefinitions.Add(
                Builders<Report>.Filter.Or(new List<FilterDefinition<Report>>() {
                        Builders<Report>.Filter.Eq("UserName", userName),
                        Builders<Report>.Filter.Eq("IsPublic", true),
                    }
                )
            );
        }

        return fieldDefinitions.Any() ? Builders<Report>.Filter.And(fieldDefinitions) : Builders<Report>.Filter.Empty;
    }

    private static SortDefinition<Report> CreateSortDefinition(string? sortField, Models.SortDirection? sortDirection)
    {
        if (string.IsNullOrEmpty(sortField) || sortDirection == Models.SortDirection.None)
        {
            return Builders<Report>.Sort.Descending("Date");
        }

        return sortDirection == Models.SortDirection.Ascending ? Builders<Report>.Sort.Ascending(sortField) : Builders<Report>.Sort.Descending(sortField);
    }
}
