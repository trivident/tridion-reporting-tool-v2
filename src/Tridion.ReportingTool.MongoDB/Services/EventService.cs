﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using Tridion.ReportingTool.Models;
using Tridion.ReportingTool.Models.Contracts;
using Tridion.ReportingTool.MongoDB.Options;

namespace Tridion.ReportingTool.MongoDB.Services;

public class EventService : IEventService
{
    private readonly MongoOptions _options;
    private readonly ILogger<EventService> _logger;
    private readonly IMongoCollection<Event> _eventCollection;

    public EventService(IMongoClient mongoClient, IOptions<MongoOptions> options, ILogger<EventService> logger)
    {
        _logger = logger;
        _options = options.Value;

        var mongoDatabase = mongoClient.GetDatabase(_options.Database);
        _eventCollection = mongoDatabase.GetCollection<Event>("events");
    }

    public async Task<Event?> GetEvent(string id, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("GetEvent() - {}", id);
        return await _eventCollection.Find(x => x.EventId == id).SingleOrDefaultAsync(cancellationToken);
    }

    public async Task<SearchResult<Event>> GetEvents(EventSearchFilter filter, int page, int pageSize, string? sortfield = null, Models.SortDirection? sortDirection = null, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("GetEvents - {},{}", page, pageSize);

        var filterDefinition = CreateFilterDefinition(filter);
        var sortDefinition = CreateSortDefinition(sortfield, sortDirection);

        var countFacet = AggregateFacet.Create("count",
            PipelineDefinition<Event, AggregateCountResult>.Create(new[]
            {
                PipelineStageDefinitionBuilder.Count<Event>()
            })
        );
        var dataFacet = AggregateFacet.Create("data",
            PipelineDefinition<Event, Event>.Create(new[]
            {
                PipelineStageDefinitionBuilder.Sort(sortDefinition),
                PipelineStageDefinitionBuilder.Skip<Event>(page * pageSize),
                PipelineStageDefinitionBuilder.Limit<Event>(pageSize),
            })
        );
        var aggregateResult = await _eventCollection.Aggregate()
            .Match(filterDefinition)
            .Facet(countFacet, dataFacet)
            .SingleAsync(cancellationToken);

        if (!aggregateResult.Facets.Any())
        {
            return new SearchResult<Event>();
        }

        var eventNames = CreateAggregateFacet("$EventName", "eventNames");
        var eventFamilies = CreateAggregateFacet("$EventFamily", "eventFamilies");
        var subjectTypes = CreateAggregateFacet("$SubjectType", "subjectTypes");
        var publications = CreateAggregateFacet("$PublicationName", "publications");
        var servers = CreateAggregateFacet("$ServerName", "servers");
        var users = CreateAggregateFacet("$UserName", "usernames");
        var facets = await _eventCollection.Aggregate()
            .Facet(eventNames, eventFamilies, subjectTypes, publications, servers, users)
            .SingleAsync(cancellationToken);

        var buckets = facets.Facets
                .Select(facet => new AggregationBucket
                {
                    Name = facet.Name,
                    Results = facet.Output<AggregateSortByCountResult<string>>()
                                    .Select(i => new AggregationResult { Count = i.Count, Key = i.Id })
                                    .ToList()
                }).ToList().AsReadOnly();

        return new SearchResult<Event>
        {
            Items = aggregateResult.Facets.First(x => x.Name == "data").Output<Event>(),
            TotalItems = (int)(aggregateResult.Facets.First(x => x.Name == "count").Output<AggregateCountResult>().FirstOrDefault()?.Count ?? 0),
            Filters = new SearchAggregationResult(buckets)
        };
    }

    public async Task<SearchAggregationResult> GetAggregatedEvents(EventSearchFilter filter, string primaryAggregationField, string? secondaryAggregationField = null, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("GetAggregatedEvents - {},{}", primaryAggregationField, secondaryAggregationField);

        var match = GetMatchFilter(filter);
        var group = BsonDocument.Parse(@$"{{ ""$group"" : {{ ""_id"" : {{ ""{primaryAggregationField}"" : ""${primaryAggregationField}"" {(string.IsNullOrEmpty(secondaryAggregationField) ? "" : $@", ""{secondaryAggregationField}"" : ""${secondaryAggregationField}""")} }}, ""Count"" : {{ ""$sum"" : 1 }} }} }}");
        var project = BsonDocument.Parse(@$"{{ ""$project"" : {{ ""_id"" : 0, ""{primaryAggregationField}"" : ""$_id.{primaryAggregationField}"" {(string.IsNullOrEmpty(secondaryAggregationField) ? "" : $@", ""{secondaryAggregationField}"" : ""$_id.{secondaryAggregationField}""")}, ""Count"" : 1 }} }}");
        var sort = BsonDocument.Parse(@$"{{ ""$sort"" : {{ ""Count"" : -1 }} }}");

        var cursor = await _eventCollection
            .AggregateAsync<BsonDocument>(match is not null ? new[] { match, group, project, sort } : new[] { group, project, sort }, cancellationToken: cancellationToken);

        var result = new List<AggregationBucket>();
        await cursor.ForEachAsync(bsonDocument =>
        {
            string primaryValue;
            int total;
            var bsonElmt = bsonDocument.FirstOrDefault(b => b.Name == primaryAggregationField);
            if (bsonElmt == default || bsonElmt.Value is BsonNull)
            {
                primaryValue = "undefined";
            }
            else
            {
                primaryValue = bsonElmt.Value.AsString;
            }

            bsonElmt = bsonDocument.FirstOrDefault(b => b.Name == "Count");
            if (bsonElmt == default || bsonElmt.Value is BsonNull)
            {
                total = -1; // NOTE: this should never happen!
            }
            else
            {
                total = bsonElmt.Value.AsInt32;
            }

            var bucket = result.FirstOrDefault(b => b.Name == primaryValue);
            if (bucket is null)
            {
                bucket = new AggregationBucket
                {
                    Name = primaryValue,
                    Total = total,
                    Results = bsonDocument
                       .Where(b => b.Name != "Count" && b.Name != primaryAggregationField)
                       .Select(b => new AggregationResult
                       {
                           Key = b.Value.AsString,
                           Count = total
                       }).ToList()
                };
                result.Add(bucket);
            }
            else
            {
                bucket.Total += total;
                bucket.Results.AddRange(bsonDocument
                    .Where(b => b.Name != "Count" && b.Name != primaryAggregationField)
                    .Select(b => new AggregationResult
                    {
                        Key = b.Value.AsString,
                        Count = total
                    }));
            }
        }, cancellationToken: cancellationToken);

        return new SearchAggregationResult(result.AsReadOnly());
    }

    public async Task<IReadOnlyCollection<string>> GetSuggestions(string query, string field, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("GetSuggestions - {},{}", query, field);
        var filter = Builders<Event>.Filter.Regex(field, new BsonRegularExpression(query, "i"));
        var result = await _eventCollection.DistinctAsync<string>(field, filter, cancellationToken: cancellationToken);
        return await result.ToListAsync(cancellationToken);
    }

    private static AggregateFacet<Event, AggregateSortByCountResult<string>> CreateAggregateFacet(string fieldName, string facetName)
    {
        return AggregateFacet.Create(facetName,
            PipelineDefinition<Event, AggregateSortByCountResult<string>>.Create(new[]
            {
                PipelineStageDefinitionBuilder.SortByCount<Event, string>(fieldName)
            })
        );
    }

    private static BsonDocument? GetMatchFilter(EventSearchFilter filter)
    {
        if (filter == null)
        {
            return null;
        }

        var filters = new List<string>();
        if (!string.IsNullOrEmpty(filter.EventName))
        {
            filters.Add($@"EventName: ""{filter.EventName}""");
        }

        if (!string.IsNullOrEmpty(filter.EventFamily))
        {
            filters.Add($@"EventFamily: ""{filter.EventFamily}""");
        }

        if (!string.IsNullOrEmpty(filter.PublicationName))
        {
            filters.Add($@"PublicationId: ""{filter.PublicationName}""");
        }

        if (!string.IsNullOrEmpty(filter.UserName))
        {
            filters.Add($@"UserId: ""{filter.UserName}""");
        }

        if (!string.IsNullOrEmpty(filter.ServerName))
        {
            filters.Add($@"ServerName: ""{filter.ServerName}""");
        }

        if (filter.ItemType is not null)
        {
            filters.Add($@"ItemType: ""{filter.ItemType}""");
        }

        if (filter.DateRange?.Start != null && filter.DateRange?.End != null)
        {

            filters.Add($@"EventStartDate: {{ $gte: {{ $date: ""{filter.DateRange?.Start}"" }}, $lte: {{ $date: ""{filter.DateRange?.End}"" }} }}");
        }
        else
        {
            if (filter.DateRange?.Start != null)
            {
                filters.Add($@"EventStartDate: {{ $gte: {{ $date: ""{filter.DateRange?.Start}"" }} }}");
            }

            if (filter.DateRange?.End != null)
            {
                filters.Add($@"EventStartDate: {{ $lte: {{ $date: ""{filter.DateRange?.End}"" }} }}");
            }
        }

        if (!string.IsNullOrEmpty(filter.Query))
        {
            filters.Add($@"$text: {{ $search: ""{filter.Query}"" }}");
        }

        if (!filters.Any())
        {
            return null;
        }

        var joined = string.Join(",", filters);
        var wrapped = "{ $match: { " + joined.Replace(@"\", @"\\") + "} }";

        return BsonDocument.Parse(wrapped);
    }

    private static SortDefinition<Event> CreateSortDefinition(string? sortField, Models.SortDirection? sortDirection)
    {
        if (string.IsNullOrEmpty(sortField) || sortDirection == Models.SortDirection.None)
        {
            return Builders<Event>.Sort.Descending("Date");
        }

        return sortDirection == Models.SortDirection.Ascending ? Builders<Event>.Sort.Ascending(sortField) : Builders<Event>.Sort.Descending(sortField);
    }

    private static FilterDefinition<Event> CreateFilterDefinition(EventSearchFilter filter)
    {
        if (filter is null)
        {
            return Builders<Event>.Filter.Empty;
        }

        var fieldDefinitions = new List<FilterDefinition<Event>>();
        if (!string.IsNullOrEmpty(filter.EventName))
        {
            fieldDefinitions.Add(Builders<Event>.Filter.Eq(e => e.EventName, filter.EventName));
        }

        if (!string.IsNullOrEmpty(filter.EventFamily))
        {
            fieldDefinitions.Add(Builders<Event>.Filter.Eq(e => e.EventFamily, filter.EventFamily));
        }

        if (!string.IsNullOrEmpty(filter.PublicationName))
        {
            fieldDefinitions.Add(Builders<Event>.Filter.Eq(e => e.PublicationName, filter.PublicationName));
        }

        if (!string.IsNullOrEmpty(filter.UserName))
        {
            fieldDefinitions.Add(Builders<Event>.Filter.Eq(e => e.UserName, filter.UserName));
        }

        if (!string.IsNullOrEmpty(filter.ServerName))
        {
            fieldDefinitions.Add(Builders<Event>.Filter.Eq(e => e.ServerName, filter.ServerName));
        }

        if (filter.ItemType is not null)
        {
            fieldDefinitions.Add(Builders<Event>.Filter.Eq(e => e.SubjectType, filter.ItemType));
        }

        if (filter.DateRange?.Start != null)
        {
            fieldDefinitions.Add(Builders<Event>.Filter.Gte(t => t.EventStartDate, filter.DateRange?.Start));
        }

        if (filter.DateRange?.End != null)
        {
            fieldDefinitions.Add(Builders<Event>.Filter.Lte(t => t.EventStartDate, filter.DateRange?.End));
        }

        if (!string.IsNullOrEmpty(filter.Query))
        {
            fieldDefinitions.Add(Builders<Event>.Filter.Text(filter.Query));
        }

        return fieldDefinitions.Any() ? Builders<Event>.Filter.And(fieldDefinitions) : Builders<Event>.Filter.Empty;
    }
}
