﻿namespace Tridion.ReportingTool.MongoDB.Options;

public record MongoOptions
{
    public string ConnectionString { get; set; }
    public string Database { get; set; }
}
