using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Tridion.ReportingTool.Api.Controllers;
using Tridion.ReportingTool.Models;
using Tridion.ReportingTool.Models.Contracts;
using Xunit;

namespace Tridion.ReportingTool.Api.Tests;
public class EventControllerTests
{
    [Fact]
    public async Task Events_Returns200_AndListOfEvents()
    {
        // Arrange
        var mockRepo = new Mock<IEventService>();
        var mockLogger = new Mock<ILogger<EventsController>>();
        mockRepo.Setup(repo => repo.GetEvents(It.IsAny<EventSearchFilter>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string?>(), It.IsAny<SortDirection>(), It.IsAny<CancellationToken>())).Returns(GetEvents());
        var controller = new EventsController(mockRepo.Object, mockLogger.Object);

        // Act
        var response = await controller.GetEvents(new EventSearchFilter(), 0, 10, "", SortDirection.None, CancellationToken.None);

        // Assert
        var result = Assert.IsType<OkObjectResult>(response); 
        Assert.Equal(200, result.StatusCode);
        Assert.IsType<SearchResult<Event>>(result.Value);
        Assert.Equal(1, (result.Value as SearchResult<Event>).Items.Count);
    }


    private async Task<SearchResult<Event>> GetEvents()
    {
        return new SearchResult<Event>
        {
            Items = new List<Event>() { new Event
                {
                    EventId = "tcm:0-0-0"
                } 
            }
        };
    }
}