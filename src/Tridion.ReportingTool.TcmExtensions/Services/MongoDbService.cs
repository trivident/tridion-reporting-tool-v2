﻿using Serilog;
using System.Threading.Tasks;
using Tridion.ContentManager;
using Tridion.ContentManager.Extensibility;
using Tridion.ContentManager.Extensibility.Events;
using Tridion.ContentManager.Publishing;

namespace Tridion.ReportingTool.TcmExtensions.Services
{
    public class MongoDbService
    {
        private readonly ILogger _logger;
        private readonly ReportingToolOptions _options;

        public MongoDbService(ReportingToolOptions options, ILogger logger)
        {
            _options = options;
            _logger = logger;
        }

        public bool ShouldLoadEvent(string eventName)
        {
            return true;
        }

        public async Task<bool> LogEvent<U,T>(U subject, T eventArgs, EventPhases phase) where U : IdentifiableObject where T : TcmEventArgs
        {
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        public async Task<bool> LogEventTimeStamp<U,T>(U subject, T eventArgs, EventPhases phase) where U : IdentifiableObject where T : TcmEventArgs
        {
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        public async Task<bool> LogPublishTransaction(PublishTransaction subject, SaveEventArgs eventArgs, EventPhases phase)
        {
            return true;
        }
    }
}
