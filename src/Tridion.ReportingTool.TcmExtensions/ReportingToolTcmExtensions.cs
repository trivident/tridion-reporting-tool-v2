﻿using Microsoft.Extensions.Configuration;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;
using Tridion.ContentManager;
using Tridion.ContentManager.AudienceManagement;
using Tridion.ContentManager.ContentManagement;
using Tridion.ContentManager.Extensibility;
using Tridion.ContentManager.Extensibility.Events;
using Tridion.ContentManager.Publishing;
using Tridion.ContentManager.Workflow;
using Tridion.ReportingTool.TcmExtensions.Services;

namespace Tridion.ReportingTool.TcmExtensions
{
    [TcmExtension("ReportingTool extensions")]
    public class ReportingToolTcmExtensions : TcmExtension
    {
        private static bool _isInitialized = false;
        private readonly ReportingToolOptions _options;
        private readonly MongoDbService _mongoDbService;
        private readonly ILogger _logger;

        private List<EventSubscription> subscriptions = new List<EventSubscription>();

        public ReportingToolTcmExtensions()
        {
            _logger = new LoggerConfiguration()
                    .ReadFrom.AppSettings()
                    .CreateLogger();

            _mongoDbService = new MongoDbService(_options, _logger);

            Subscribe();
        }

        private void Subscribe()
        {
            if (_mongoDbService.ShouldLoadEvent("PublishOrUnPublishEventArgs"))
            {
                subscriptions.Add(EventSystem.SubscribeAsync<PublishTransaction, SaveEventArgs>(LogPublishTransaction, EventPhases.Initiated));
                subscriptions.Add(EventSystem.Subscribe<IdentifiableObject, PublishOrUnPublishEventArgs>(LogEvent, EventPhases.Initiated));
                subscriptions.Add(EventSystem.Subscribe<IdentifiableObject, PublishOrUnPublishEventArgs>(LogEventTimeStamp, EventPhases.TransactionCommitted | EventPhases.TransactionAborted | EventPhases.TransactionInDoubt));
            }

            if (_mongoDbService.ShouldLoadEvent("VersioningEventArgs"))
            {
                subscriptions.Add(EventSystem.Subscribe<IdentifiableObject, VersioningEventArgs>(LogEvent, EventPhases.Initiated));
                subscriptions.Add(EventSystem.Subscribe<IdentifiableObject, VersioningEventArgs>(LogEventTimeStamp, EventPhases.TransactionCommitted | EventPhases.TransactionAborted | EventPhases.TransactionInDoubt));
            }

            if (_mongoDbService.ShouldLoadEvent("BlueprintingEventArgs"))
            {
                subscriptions.Add(EventSystem.Subscribe<IdentifiableObject, PromoteOrDemoteEventArgs>(LogEvent, EventPhases.Initiated));
                subscriptions.Add(EventSystem.Subscribe<IdentifiableObject, PromoteOrDemoteEventArgs>(LogEventTimeStamp, EventPhases.TransactionCommitted | EventPhases.TransactionAborted | EventPhases.TransactionInDoubt));

                subscriptions.Add(EventSystem.Subscribe<IdentifiableObject, LocalizeEventArgs>(LogEvent, EventPhases.Initiated));
                subscriptions.Add(EventSystem.Subscribe<IdentifiableObject, LocalizeEventArgs>(LogEventTimeStamp, EventPhases.TransactionCommitted | EventPhases.TransactionAborted | EventPhases.TransactionInDoubt));

                subscriptions.Add(EventSystem.Subscribe<IdentifiableObject, UnLocalizeEventArgs>(LogEvent, EventPhases.Initiated));
                subscriptions.Add(EventSystem.Subscribe<IdentifiableObject, UnLocalizeEventArgs>(LogEventTimeStamp, EventPhases.TransactionCommitted | EventPhases.TransactionAborted | EventPhases.TransactionInDoubt));
            }

            if (_mongoDbService.ShouldLoadEvent("CrudEventArgs"))
            {
                subscriptions.Add(EventSystem.Subscribe<IdentifiableObject, CopyEventArgs>(LogEvent, EventPhases.Initiated));
                subscriptions.Add(EventSystem.Subscribe<IdentifiableObject, CopyEventArgs>(LogEventTimeStamp, EventPhases.TransactionCommitted | EventPhases.TransactionAborted | EventPhases.TransactionInDoubt));

                subscriptions.Add(EventSystem.Subscribe<IdentifiableObject, CopyToKeywordEventArgs>(LogEvent, EventPhases.Initiated));
                subscriptions.Add(EventSystem.Subscribe<IdentifiableObject, CopyToKeywordEventArgs>(LogEventTimeStamp, EventPhases.TransactionCommitted | EventPhases.TransactionAborted | EventPhases.TransactionInDoubt));

                subscriptions.Add(EventSystem.Subscribe<IdentifiableObject, DeleteEventArgs>(LogEvent, EventPhases.Initiated));
                subscriptions.Add(EventSystem.Subscribe<IdentifiableObject, DeleteEventArgs>(LogEventTimeStamp, EventPhases.TransactionCommitted | EventPhases.TransactionAborted | EventPhases.TransactionInDoubt));

                subscriptions.Add(EventSystem.Subscribe<IdentifiableObject, MoveToKeywordEventArgs>(LogEvent, EventPhases.Initiated));
                subscriptions.Add(EventSystem.Subscribe<IdentifiableObject, MoveToKeywordEventArgs>(LogEventTimeStamp, EventPhases.TransactionCommitted | EventPhases.TransactionAborted | EventPhases.TransactionInDoubt));

                // the save event on VersionedItems has problems and duplicates the checkin event
                // we will not log save on RLOs anymore, instead we will subscribe to all derived types
                // of RepositoryLocalObject that are NOT a versioned item
                //Subscribe<RepositoryLocalObject, SaveEventArgs>();
                subscriptions.Add(EventSystem.Subscribe<Keyword, SaveEventArgs>(LogEvent, EventPhases.Initiated));
                subscriptions.Add(EventSystem.Subscribe<Keyword, SaveEventArgs>(LogEventTimeStamp, EventPhases.TransactionCommitted | EventPhases.TransactionAborted | EventPhases.TransactionInDoubt));


                subscriptions.Add(EventSystem.Subscribe<OrganizationalItem, SaveEventArgs>(LogEvent, EventPhases.Initiated));
                subscriptions.Add(EventSystem.Subscribe<OrganizationalItem, SaveEventArgs>(LogEventTimeStamp, EventPhases.TransactionCommitted | EventPhases.TransactionAborted | EventPhases.TransactionInDoubt));

                subscriptions.Add(EventSystem.Subscribe<ProcessDefinition, SaveEventArgs>(LogEvent, EventPhases.Initiated));
                subscriptions.Add(EventSystem.Subscribe<ProcessDefinition, SaveEventArgs>(LogEventTimeStamp, EventPhases.TransactionCommitted | EventPhases.TransactionAborted | EventPhases.TransactionInDoubt));

                subscriptions.Add(EventSystem.Subscribe<TargetGroup, SaveEventArgs>(LogEvent, EventPhases.Initiated));
                subscriptions.Add(EventSystem.Subscribe<TargetGroup, SaveEventArgs>(LogEventTimeStamp, EventPhases.TransactionCommitted | EventPhases.TransactionAborted | EventPhases.TransactionInDoubt));
            }

            // subscribe to a housekeeping event, designed to allow processed from the outside to communicate
            // with this event system
            subscriptions.Add(EventSystem.Subscribe<MultimediaType, SaveEventArgs>(ReinitializeEvents, EventPhases.Processed));
        }


        private void ReinitializeEvents(MultimediaType subject, SaveEventArgs e, EventPhases phase)
        {
            StringBuilder sb = new StringBuilder();
            if (subject.Id == "tcm:0-1-65544")
            {
                foreach (var subscription in subscriptions)
                {
                    sb.AppendLine($"- subject {subscription.SubjectType}, event {subscription.EventType} and phases {subscription.Phases}");
                    subscription.Unsubscribe();
                }
                subscriptions.Clear();
                _logger.Information("unsubscribed from: \r\n" + sb.ToString());
                Subscribe();
            }
        }

        /// <summary>
        /// Bind settings to the configuration file
        /// </summary>
        private void Initialize()
        {
            AddonConfiguration.GetSection("configuration:Tridion.ReportingTool.TcmExtensions").Bind(_options);
            _isInitialized = true;
        }

        private async void LogEvent<U, T>(U subject, T eventArgs, EventPhases phase) where U : IdentifiableObject where T : TcmEventArgs
        {
            try
            {
                _logger.Debug("{type} - {phase} phase", typeof(T).FullName, Enum.GetName(typeof(EventPhases), phase));
                await _mongoDbService.LogEvent(subject, eventArgs, phase);
            }
            catch (Exception e)
            {
                _logger.Error(e, "caught exception while trying to log an event [subject: {id}, event: {args}, phase: {phase}]", subject.Id, nameof(eventArgs), phase);
            }
        }

        private async void LogEventTimeStamp<U, T>(U subject, T eventArgs, EventPhases phase) where U : IdentifiableObject where T : TcmEventArgs
        {
            try
            {
                _logger.Debug("{type} - {phase} phase", typeof(T).FullName, Enum.GetName(typeof(EventPhases), phase));
                await _mongoDbService.LogEventTimeStamp(subject, eventArgs, phase);
            }
            catch (Exception e)
            {
                _logger.Error(e, "caught exception while trying to log an event [subject: {id}, event: {args}, phase: {phase}]", subject.Id, nameof(eventArgs), phase);
            }
        }

        private async void LogPublishTransaction(PublishTransaction subject, SaveEventArgs eventArgs, EventPhases phase)
        {
            try
            {
                _logger.Debug("Publish {state} phase", Enum.GetName(typeof(PublishTransactionState), subject.State));
                await _mongoDbService.LogPublishTransaction(subject, eventArgs, phase);
            }
            catch (Exception e)
            {
                _logger.Error(e, "caught exception while trying to log an event [subject: {id}, event: {args}, phase: {phase}]", subject.Id, nameof(eventArgs), phase);
            }

        }
    }
}
