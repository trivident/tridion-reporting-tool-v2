﻿using Microsoft.Extensions.Logging;
using Swashbuckle.Swagger.Annotations;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using Tridion.CoreService.Legacy.Models;

namespace Tridion.CoreService.Legacy.Api.Controllers
{
    [RoutePrefix("api/tridion")]
    public class CoreServiceController : ApiController
    {
        private readonly ICoreService _coreService;
        private readonly ILogger<CoreServiceController> _logger;

        public CoreServiceController(ICoreService coreService, ILogger<CoreServiceController> logger)
        {
            _coreService = coreService;
            _logger = logger;
        }

        /// <summary>
        /// Trigger a save to resubscribe to Tridion events.
        /// </summary>
        /// <param name="id">Tcm uri to save</param>
        /// <response code="202">Accepted</response>
        /// <response code="400">Bad request if the tcm uri does not exists</response>  
        [HttpPost]
        [Route("resubscribe")]
        [SwaggerResponse(HttpStatusCode.Accepted)]
        [SwaggerResponse(HttpStatusCode.BadRequest)]

        public IHttpActionResult Resubscribe(string id)
        {
            _logger.LogTrace("Triggering Resubscribe {}", id);
            var result = _coreService.Resubscribe(id);
            return result ? StatusCode(HttpStatusCode.Accepted) : StatusCode(HttpStatusCode.BadRequest);
        }

        /// <summary>
        /// Request all usergroups available in Tridion.
        /// </summary>
        /// <response code="200">OK</response>
        [HttpGet]
        [Route("usergroups")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(IReadOnlyCollection<UserGroup>))]
        public IReadOnlyCollection<UserGroup> GetUserGroups()
        {
            _logger.LogTrace("GetUserGroups");
            return _coreService.GetUserGroups();
        }

        /// <summary>
        /// Request the usergroups of a single user by id or username.
        /// </summary>
        /// <param name="user">User</param>
        /// <response code="200">OK</response>
        [HttpPost]
        [Route("usergroups/user")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(UserResponse))]
        public UserResponse GetUser([FromBody] UserRequest user)
        {
            _logger.LogTrace("GetUser(), userName: {}", user.UserName);
            return _coreService.GetUser(user);
        }
    }
}
