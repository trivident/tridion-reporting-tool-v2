﻿using Autofac;
using Autofac.Integration.WebApi;
using Serilog;
using Serilog.Extensions.Autofac.DependencyInjection;
using System.Reflection;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Mvc;
using Tridion.CoreService.Legacy.Options;

namespace Tridion.CoreService.Legacy.Api
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);

            // Create your builder.
            var builder = new ContainerBuilder();

            var loggerConfiguration = new LoggerConfiguration()
                .ReadFrom.AppSettings();
            builder.RegisterSerilog(loggerConfiguration);

            var options = new TridionOptions()
            {
                Hostname = WebConfigurationManager.AppSettings["Tridion.Hostname"],
                Domain = WebConfigurationManager.AppSettings["Tridion.Domain"],
                Username = WebConfigurationManager.AppSettings["Tridion.Username"],
                Password = WebConfigurationManager.AppSettings["Tridion.Password"],
            };
            //builder.RegisterInstance<ICoreService>(new CoreService(options));
            builder.RegisterInstance(options).As<TridionOptions>();
            builder.RegisterType<CoreService>().As<ICoreService>();

            // Register Controllers
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            var container = builder.Build();

            // Use Autofac to discover controllers
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            GlobalConfiguration.Configure(SwaggerConfig.Register);
        }
    }
}
