﻿using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using System.Reflection;
using Tridion.ReportingTool.Models;
using Tridion.ReportingTool.Models.Contracts;

namespace Tridion.ReportingTool.Export.Excel.Services;

public class ExportService : IExportService
{
    private readonly IEventService _eventService;
    private readonly ILogger<ExportService> _logger;

    public ExportService(IEventService eventService, ILogger<ExportService> logger)
    {
        _eventService = eventService;
        _logger = logger;
    }

    public async Task<Stream> Export(EventSearchFilter filter, CancellationToken cancellationToken = default)
    {
        var data = await _eventService.GetEvents(filter, 0, int.MaxValue, cancellationToken: cancellationToken);

        ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
        using var package = new ExcelPackage();
        var worksheet = package.Workbook.Worksheets.Add("TridionEvents");

        // TODO add attribute to mark 'exportable' properties (or use custom model)
        //var properties = data.Select(e => e.GetType()).Distinct()
        //    .SelectMany(e => e.GetExportableProperties()).Distinct(new PropertyNameComparer())
        //    .ToArray();
        var properties = typeof(Event).GetProperties().Distinct().ToArray();

        worksheet.Cells[1, 1].LoadFromCollection(data.Items, true, TableStyles.None, BindingFlags.Instance | BindingFlags.Public, properties);
        int colNumber = 1;
        foreach (var property in properties)
        {
            if (property.PropertyType == typeof(DateTime))
            {
                worksheet.Column(colNumber).Style.Numberformat.Format = "YYYY-MM-DD HH:mm";
            }
            colNumber++;
        }
        worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();

        return new MemoryStream(await package.GetAsByteArrayAsync(cancellationToken));
    }

    public async Task<Stream> ExportAggregated(EventSearchFilter filter, string primaryAggregationField, string? secondaryAggregationField, CancellationToken cancellationToken = default)
    {
        var data = await _eventService.GetAggregatedEvents(filter, primaryAggregationField, secondaryAggregationField, cancellationToken);

        var dynamicColumnNames = new List<string>();
        foreach (var row in data.Buckets)
        {
            foreach (var col in row.Results)
            {
                if (!dynamicColumnNames.Contains(col.Key))
                {
                    dynamicColumnNames.Add(col.Key);
                }
            }
        }

        ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
        using var package = new ExcelPackage();
        var worksheet = package.Workbook.Worksheets.Add("TridionEvents");
        worksheet.Cells[1, 1].Value = primaryAggregationField;

        var colIndex = 2;
        if (!string.IsNullOrWhiteSpace(secondaryAggregationField))
        {
            foreach (var col in dynamicColumnNames)
            {
                worksheet.Cells[1, colIndex].Value = col;
                colIndex++;
            }
        }
        worksheet.Cells[1, colIndex].Value = "Total";

        var rowIndex = 2;


        if (!string.IsNullOrWhiteSpace(secondaryAggregationField))
        {
            foreach (var row in data.Buckets)
            {
                var total = 0;
                colIndex = 2;

                worksheet.Cells[rowIndex, 1].Value = row.Name;

                foreach (var dynamicCol in dynamicColumnNames)
                {
                    var val = row.Results.Any(r => r.Key == dynamicCol) ? (int) row.Results.First(r => r.Key == dynamicCol).Count : 0;
                    total += val;
                    worksheet.Cells[rowIndex, colIndex].Value = val;
                    colIndex++;
                }
                worksheet.Cells[rowIndex, colIndex].Value = total;
                rowIndex++;
            }
        }
        else
        {
            foreach (var row in data.Buckets)
            {
                worksheet.Cells[rowIndex, 1].Value = row.Name;
                worksheet.Cells[rowIndex, 2].Value = row.Total;
                rowIndex++;
            }
        }

        //worksheet.Cells[1, 1].LoadFromCollection(data, true);
        worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();

        return new MemoryStream(await package.GetAsByteArrayAsync(cancellationToken));
    }
}
