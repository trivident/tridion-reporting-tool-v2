﻿using Microsoft.Extensions.DependencyInjection;
using Tridion.ReportingTool.Export.Excel.Services;
using Tridion.ReportingTool.Models.Contracts;

namespace Tridion.ReportingTool.Export.Excel.Extensions;

public static class ServiceCollectionExtensions
{
    public static void UseExcelExport(this IServiceCollection services)
    {
        services.AddScoped<IExportService, ExportService>();
    }
}
