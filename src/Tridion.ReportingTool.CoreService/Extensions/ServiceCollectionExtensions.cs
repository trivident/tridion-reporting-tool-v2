﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Tridion.CoreService.Legacy.Client.Extensions;
using Tridion.CoreService.Rest.Extensions;
using Tridion.ReportingTool.CoreService.Services;
using Tridion.ReportingTool.Models.Contracts;

namespace Tridion.ReportingTool.CoreService.Extensions;

public static class ServiceCollectionExtensions
{
    public static void UseCoreServiceRest(this IServiceCollection services, IConfiguration configuration)
    {
        services.UseCoreServiceRestClient(configuration);
        services.AddScoped<ICoreService, TridionCoreServiceRest>();
    }

    public static void UseCoreServiceLegacy(this IServiceCollection services, IConfiguration configuration)
    {
        services.UseLegacyCoreServiceClient(configuration);
        services.AddScoped<ICoreService, TridionCoreServiceLegacy>();
    }
}
