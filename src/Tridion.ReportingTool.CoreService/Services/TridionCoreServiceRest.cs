﻿using Microsoft.Extensions.Logging;
using Tridion.CoreService.Rest;
using Tridion.ReportingTool.Models;
using Tridion.ReportingTool.Models.Contracts;

namespace Tridion.ReportingTool.CoreService.Services;

// TODO escape tcm ids
public class TridionCoreServiceRest : ICoreService
{
    private readonly ICoreServiceClient _client;
    private readonly ILogger<TridionCoreServiceRest> _logger;

    public TridionCoreServiceRest(ICoreServiceClient client, ILogger<TridionCoreServiceRest> logger)
    {
        _client = client;
        _logger = logger;
    }

    public Task<Models.User> GetUser(LoginRequest loginRequest, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("GetUserGroupsFromUserName() - {}", loginRequest.UserName);
        throw new NotImplementedException();
    }

    public async Task<IReadOnlyCollection<UserGroup>> GetUserGroups(CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("GetUserGroups()");
        throw new NotImplementedException();
    }


    public async Task Resubscribe(string id = "tcm:0-1-65544", CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("Resubscribe() - {}", id);
        await _client.CheckInAsync(id, new CheckInRequest(), cancellationToken: cancellationToken);
    }
}
