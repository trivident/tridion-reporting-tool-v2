﻿using Microsoft.Extensions.Logging;
using Tridion.CoreService.Legacy.Client;
using Tridion.ReportingTool.Models;
using Tridion.ReportingTool.Models.Contracts;

namespace Tridion.ReportingTool.CoreService.Services;

public class TridionCoreServiceLegacy : ICoreService
{
    private readonly ICoreServiceClient _client;
    private readonly ILogger<TridionCoreServiceLegacy> _logger;

    public TridionCoreServiceLegacy(ICoreServiceClient client, ILogger<TridionCoreServiceLegacy> logger)
    {
        _client = client;
        _logger = logger;
    }

    public async Task<IReadOnlyCollection<Models.UserGroup>> GetUserGroups(CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("GetUserGroups()");
        var result = await _client.GetUserGroupsAsync(cancellationToken);
        return result.Select(u => new Models.UserGroup { Id = u.Id, Name = u.Name }).ToList().AsReadOnly();
    }

    public async Task<Models.User> GetUser(LoginRequest loginRequest, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("GetUserGroupsFromUserName() - {}", loginRequest.UserName);
        var result = await _client.GetUserAsync(new UserRequest
        {
            Password = loginRequest.Password,
            UserName = loginRequest.UserName
        }, cancellationToken: cancellationToken);

        return new User
        {
            Name = result.UserName,
            Id = result.Id,
            Groups = result.Groups.Select(u => new Models.UserGroup { Id = u.Id, Name = u.Name }).ToList().AsReadOnly()
        };
    }

    public async Task Resubscribe(string id = "tcm:0-1-65544", CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("Resubscribe() - {}", id);
        await _client.ResubscribeAsync(id, cancellationToken);
    }
}
