﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Net;

namespace Tridion.CoreService.Rest.Extensions;

public static class ServiceCollectionExtensions
{
    public static void UseCoreServiceRestClient(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddHttpClient<ICoreServiceClient, CoreServiceClient>()
            .ConfigureHttpClient((httpClient) =>
            {
                httpClient.BaseAddress = new Uri(configuration["Tridion:BaseUrl"]);
            })
            .ConfigurePrimaryHttpMessageHandler(_ => new HttpClientHandler
            {
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate,
                UseCookies = false,
                AllowAutoRedirect = false,
                Credentials = new NetworkCredential(configuration["Tridion:UserName"], configuration["Tridion:Password"])
            });
    }
}

