﻿namespace Tridion.CoreService.Rest;

public partial class CoreServiceClient
{
    private const string ApiVersion = "1.0";
    partial void PrepareRequest(System.Net.Http.HttpClient client, System.Net.Http.HttpRequestMessage request, System.Text.StringBuilder urlBuilder)
    {
        urlBuilder.Replace("{api-version}", Uri.EscapeDataString(ApiVersion));
    }
}

