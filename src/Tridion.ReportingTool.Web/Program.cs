using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Serilog;
using System.Net.Http.Headers;
using Tridion.ReportingTool.Models.Authorization;
using Tridion.ReportingTool.Models.Contracts;
using Tridion.ReportingTool.Web;
using Tridion.ReportingTool.Web.Services;
using Tridion.ReportingTool.Web.Services.Handlers;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

// TODO add IStartupFilter to validate configuration?

Log.Logger = new LoggerConfiguration()
    .ReadFrom.Configuration(builder.Configuration)
    .CreateLogger();

builder.Logging.AddSerilog();

builder.Services.AddAntDesign();

builder.Services.AddSingleton<JwtAuthenticationStateProvider>();
// Using a factory function ensures there is only one single object for both type.
builder.Services.AddSingleton<AuthenticationStateProvider>(provider => provider.GetRequiredService<JwtAuthenticationStateProvider>());

builder.Services.AddSingleton(provider => new JwtTokenHandler(provider.GetRequiredService<JwtAuthenticationStateProvider>()));

builder.Services
    .AddHttpClient<IReportingToolClient, ReportingToolClient>()
    .ConfigureHttpClient((httpClient) =>
    {
        httpClient.BaseAddress = new Uri(builder.Configuration["TRT:BaseUrl"]);
        httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
    })
    .AddHttpMessageHandler<JwtTokenHandler>();

builder.Services.AddAuthorizationCore(options =>
{
    options.AddPolicy(Policies.IsAdmin, policyBuilder =>
    {
        policyBuilder.RequireAuthenticatedUser();
        policyBuilder.RequireRole(builder.Configuration["Tridion:AdminUserGroup"]);
    });
    options.AddPolicy(Policies.IsUser, policyBuilder =>
    {
        policyBuilder.RequireAuthenticatedUser();
        policyBuilder.RequireRole(builder.Configuration["Tridion:ReadUserGroup"]);
    });
});

var application = builder.Build();
//await RefreshJwtToken(application);
await application.RunAsync();

//static async Task RefreshJwtToken(WebAssemblyHost application)
//{
//    using var boostrapScope = application.Services.CreateScope();
//    var api = boostrapScope.ServiceProvider.GetRequiredService<IReportingToolClient>();

//    var response = await api.Authenticate(new Tridion.ReportingTool.Models.LoginRequest("Administrator", "Tr1v1d3nt"));
//    if (response is not null && response.IsSuccess)
//    {
//        var loginStateService = boostrapScope.ServiceProvider.GetRequiredService<JwtAuthenticationStateProvider>();
//        loginStateService.Login(response.Token);
//    }
//}