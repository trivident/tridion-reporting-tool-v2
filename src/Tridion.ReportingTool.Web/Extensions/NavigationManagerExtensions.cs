﻿using Microsoft.AspNetCore.Components;
using System.Collections.Specialized;
using System.Web;

namespace Tridion.ReportingTool.Web.Extensions;

public static class NavigationManagerExtensions
{
    /// <summary>
    /// get entire querystring name/value collection
    /// </summary>
    public static NameValueCollection QueryString(this NavigationManager navigationManager)
    {
        return HttpUtility.ParseQueryString(new Uri(navigationManager.Uri).Query);
    }

    /// <summary>
    /// Get single querystring value with specified key
    /// </summary>
    public static string? QueryString(this NavigationManager navigationManager, string key)
    {
        return navigationManager.QueryString()[key];
    }
}
