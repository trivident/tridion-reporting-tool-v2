﻿using System.Net.Http.Headers;

namespace Tridion.ReportingTool.Web.Services.Handlers;

public class JwtTokenHandler : DelegatingHandler
{
    private readonly JwtAuthenticationStateProvider _authenticationStateProvider;

    public JwtTokenHandler(JwtAuthenticationStateProvider authenticationStateProvider)
    {
        _authenticationStateProvider = authenticationStateProvider;
    }

    protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
    {
        // TODO: exclude from authenticate call?
        request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", _authenticationStateProvider.Token ?? string.Empty);
        return base.SendAsync(request, cancellationToken);
    }
}