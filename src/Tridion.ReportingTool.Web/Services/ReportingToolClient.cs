using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using Tridion.ReportingTool.Models;
using Tridion.ReportingTool.Models.Configuration;
using Tridion.ReportingTool.Models.Contracts;

namespace Tridion.ReportingTool.Web.Services;

public class ReportingToolClient : IReportingToolClient
{
    private readonly HttpClient _httpClient;
    private readonly ILogger<ReportingToolClient> _logger;
    private readonly JsonSerializerOptions _options;

    public ReportingToolClient(HttpClient httpClient, ILogger<ReportingToolClient> logger)
    {
        _httpClient = httpClient;
        _logger = logger;

        _options = new JsonSerializerOptions()
        {
            AllowTrailingCommas = true,
            PropertyNameCaseInsensitive = true,
        };
        _options.Converters.Add(new JsonStringEnumConverter());
    }

    public async Task<LoginResult?> Authenticate(LoginRequest loginRequest, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("Authenticate()");

        try
        {
            var response = await _httpClient.PostAsJsonAsync("api/authentication", loginRequest, cancellationToken).ConfigureAwait(false);
            return await response.Content.ReadFromJsonAsync<LoginResult?>(_options, cancellationToken).ConfigureAwait(false);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Error in Authenticate.");
        }

        return default;
    }

    public async Task<DeleteResult?> DeleteReport(Guid? id = null, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("DeleteReport() - {}", id);

        try
        {
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Delete,
                RequestUri = new Uri(_httpClient.BaseAddress, $"api/report?id={id}"),
            };
            var response = await _httpClient.SendAsync(request, cancellationToken).ConfigureAwait(false);
            return await response.Content.ReadFromJsonAsync<DeleteResult>(_options, cancellationToken).ConfigureAwait(false);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Error in DeleteReport.");
        }
        return default;
    }

    public async Task DeleteUserGroup(UserGroupConfiguration? body = null, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("DeleteUserGroup() - {}", body);

        try
        {
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Delete,
                RequestUri = new Uri("/api/configuration/usergroups"),
                Content = new StringContent(JsonSerializer.Serialize(body, _options), Encoding.UTF8, "application/json")
            };
            await _httpClient.SendAsync(request, cancellationToken).ConfigureAwait(false);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Error in DeleteUserGroup.");
        }
    }

    public async Task DisableEvent(string? eventName = null, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("DisableEvent() - {}", eventName);

        try
        {
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri(_httpClient.BaseAddress, $"api/configuration/events/disable?eventName={eventName}"),
            };
            await _httpClient.SendAsync(request, cancellationToken).ConfigureAwait(false);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Error in DisableEvent.");
        }
    }

    public async Task DisableUserGroup(string? userGroupId = null, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("DisableUserGroup() - {}", userGroupId);

        try
        {
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri(_httpClient.BaseAddress, $"api/configuration/usergroups/disable?eventName={userGroupId}"),
            };
            await _httpClient.SendAsync(request, cancellationToken).ConfigureAwait(false);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Error in DisableUserGroup.");
        }
    }

    public async Task EnableEvent(string? eventName = null, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("EnableEvent() - {}", eventName);

        try
        {
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri(_httpClient.BaseAddress, $"api/configuration/events/enable?eventName={eventName}"),
            };
            await _httpClient.SendAsync(request, cancellationToken).ConfigureAwait(false);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Error in EnableEvent.");
        }
    }

    public async Task EnableUserGroup(string? userGroupId = null, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("EnableUserGroup() - {}", userGroupId);

        try
        {
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri(_httpClient.BaseAddress, $"api/configuration/usergroups/enable?eventName={userGroupId}"),
            };
            await _httpClient.SendAsync(request, cancellationToken).ConfigureAwait(false);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Error in EnableUserGroup.");
        }
    }

    public async Task<SearchAggregationResult?> GetAggregatedEvents(string? primaryAggregationField = null, string? secondaryAggregationField = null, EventSearchFilter? body = null, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("GetAggregatedEvents() - {},{},{}", primaryAggregationField, secondaryAggregationField, body);

        try
        {
            var response = await _httpClient.PostAsJsonAsync($"api/events/aggregate?primaryAggregationField={primaryAggregationField}&secondaryAggregationField={secondaryAggregationField}", body, cancellationToken).ConfigureAwait(false);
            return await response.Content.ReadFromJsonAsync<SearchAggregationResult>(_options, cancellationToken).ConfigureAwait(false);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Error in GetAggregatedEvents.");
            return default;
        }
    }

    public async Task<Stream?> GetAggregatedExport(string? primaryAggregationField = null, string? secondaryAggregationField = null, EventSearchFilter? body = null, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("GetAggregatedExport() - {},{},{}", primaryAggregationField, secondaryAggregationField, body);

        try
        {
            var response = await _httpClient.PostAsJsonAsync($"api/export/aggregate?primaryAggregationField={primaryAggregationField}&secondaryAggregationField={secondaryAggregationField}", body, cancellationToken).ConfigureAwait(false);
            return await response.Content.ReadAsStreamAsync(cancellationToken).ConfigureAwait(false);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Error in GetAggregatedExport.");
            return default;
        }
    }

    public async Task<Event?> GetEvent(string id, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("GetEvent() - {}", id);

        try
        {
            var response = await _httpClient.PostAsJsonAsync($"api/events/{id}", cancellationToken).ConfigureAwait(false);
            return await response.Content.ReadFromJsonAsync<Event>(_options, cancellationToken).ConfigureAwait(false);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Error in GetEvent.");
            return default;
        }
    }

    public async Task<SearchResult<Event>?> GetEvents(int? page = null, int? pageSize = null, string? sortfield = null, SortDirection? sortdirection = null, EventSearchFilter? body = null, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("GetEvents() - {},{},{},{},{}", page, pageSize, sortfield, sortdirection, body);

        try
        {
            var response = await _httpClient.PostAsJsonAsync($"api/events?page={page}&pageSize={pageSize}&sortfield={sortfield}&sortdirection={sortdirection}", body, cancellationToken).ConfigureAwait(false);
            return await response.Content.ReadFromJsonAsync<SearchResult<Event>>(_options, cancellationToken).ConfigureAwait(false);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Error in GetEvents.");
            return default;
        }
    }

    public async Task<IEnumerable<EventConfiguration>?> GetEventConfigurations(CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("GetEventConfigurations()");

        try
        {
            return await _httpClient.GetFromJsonAsync<IEnumerable<EventConfiguration>>($"api/configuration/events", _options, cancellationToken).ConfigureAwait(false);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Error in GetEventConfigurations.");
            return default;
        }
    }

    public async Task<Stream?> GetExport(EventSearchFilter? body = null, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("GetExport() - {}", body);

        try
        {
            var response = await _httpClient.PostAsJsonAsync($"api/export", body, cancellationToken).ConfigureAwait(false);
            return await response.Content.ReadAsStreamAsync(cancellationToken).ConfigureAwait(false);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Error in GetExport.");
            return default;
        }
    }

    public async Task<Report?> GetReport(Guid? id = null, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("GetReport() - {}", id);

        try
        {
            return await _httpClient.GetFromJsonAsync<Report>($"api/report?id={id}", _options, cancellationToken).ConfigureAwait(false);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Error in GetReport.");
            return default;
        }
    }

    public async Task<SearchResult<Report>?> GetReports(int? page = null, int? pageSize = null, string? sortfield = null, SortDirection? sortdirection = null, ReportSearchFilter? body = null, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("GetReports() - {},{},{},{},{}", page, pageSize, sortfield, sortdirection, body);

        try
        {
            var response = await _httpClient.PostAsJsonAsync($"api/report?page={page}&pageSize={pageSize}&sortfield={sortfield}&sortdirection={sortdirection}", body, cancellationToken).ConfigureAwait(false);
            return await response.Content.ReadFromJsonAsync<SearchResult<Report>>(_options, cancellationToken).ConfigureAwait(false);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Error in GetReports.");
            return default;
        }
    }
    public async Task<IEnumerable<string>?> GetEventSuggestions(string? query = null, string? field = null, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("GetSuggestions() - {},{}", query, field);

        try
        {
            var response = await _httpClient.PostAsync($"api/events/suggestions?query={query}&field={field}", null, cancellationToken).ConfigureAwait(false);
            return await response.Content.ReadFromJsonAsync<IReadOnlyCollection<string>>(_options, cancellationToken).ConfigureAwait(false);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Error in GetSuggestions.");
            return default;
        }
    }
    public async Task<IEnumerable<string>?> GetReportSuggestions(string? query = null, string? field = null, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("GetSuggestions() - {},{}", query, field);

        try
        {
            var response = await _httpClient.PostAsync($"api/report/suggestions?query={query}&field={field}", null, cancellationToken).ConfigureAwait(false);
            return await response.Content.ReadFromJsonAsync<IReadOnlyCollection<string>>(_options, cancellationToken).ConfigureAwait(false);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Error in GetSuggestions.");
            return default;
        }
    }

    public async Task<IEnumerable<UserGroupConfiguration>?> GetUserGroupConfigurations(CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("GetUserGroupConfigurations()");

        try
        {
            return await _httpClient.GetFromJsonAsync<IEnumerable<UserGroupConfiguration>>($"api/configuration/usergroups", cancellationToken).ConfigureAwait(false);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Error in GetUserGroupConfigurations.");
            return default;
        }
    }

    public async Task<IEnumerable<UserGroup>?> GetUserGroups(CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("GetUserGroups()");

        try
        {
            return await _httpClient.GetFromJsonAsync<IEnumerable<UserGroup>>("api/tridion/usergroups", _options, cancellationToken).ConfigureAwait(false);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Error in GetUserGroups");
            return default;
        }
    }

    public async Task<Report?> InsertReport(Report? body = null, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("InsertReport() - {}", body);

        try
        {
            var response = await _httpClient.PutAsJsonAsync("api/report", body, _options, cancellationToken).ConfigureAwait(false);
            return await response.Content.ReadFromJsonAsync<Report?>(_options, cancellationToken).ConfigureAwait(false);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Error in InsertReport");
            return null;
        }
    }

    public async Task InsertUserGroup(UserGroupConfiguration? body = null, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("InsertUserGroup() - {}", body);

        try
        {
            await _httpClient.PutAsJsonAsync("api/configuration/usergroups", body, _options, cancellationToken).ConfigureAwait(false);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Error in InsertUserGroup.");
        }
    }

    public async Task RemoveUser(string? userName = null, CancellationToken cancellationToken = default)
    {
        throw new NotImplementedException();
    }

    public async Task Resubscribe(string? id = null, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("Resubscribe() - {}", id);

        try
        {
            await _httpClient.PostAsJsonAsync("api/tridion/resubscribe", id, cancellationToken).ConfigureAwait(false);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Error in Resubscribe.");
        }
    }

    public async Task UpdateReport(Report? body = null, CancellationToken cancellationToken = default)
    {
        _logger.LogTrace("UpdateReport() - {}", body);

        try
        {
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Patch,
                RequestUri = new Uri(_httpClient.BaseAddress, "api/report"),
                Content = new StringContent(JsonSerializer.Serialize(body, _options), Encoding.UTF8, "application/json")
            };
            await _httpClient.SendAsync(request, cancellationToken).ConfigureAwait(false);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Error in UpdateReport");
        }
    }
}
