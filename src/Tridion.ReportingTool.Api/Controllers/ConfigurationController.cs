using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using Tridion.ReportingTool.Models.Authorization;
using Tridion.ReportingTool.Models.Configuration;
using Tridion.ReportingTool.Models.Contracts;

namespace Tridion.ReportingTool.Api.Controllers;

[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Policy = Policies.IsAdmin)]
[ApiController]
[Route("api/[controller]")]
public class ConfigurationController : ControllerBase
{
    private readonly IConfigurationService _configurationService;
    private readonly ILogger<ConfigurationController> _logger;

    public ConfigurationController(IConfigurationService configurationService, ILogger<ConfigurationController> logger)
    {
        _configurationService = configurationService;
        _logger = logger;
    }

    /// <summary>
    /// Delete a usergroup from the configuration.
    /// </summary>
    /// <param name="userGroup">Usergroup to delete</param>
    /// <response code="202">Accepted</response>
    [HttpDelete("usergroups", Name = "Delete user group")]
    [SwaggerResponse(202)]
    public async Task<IActionResult> Delete(UserGroupConfiguration userGroup, CancellationToken cancellationToken)
    {
        _logger.LogTrace("Delete(), {}", userGroup.UserGroupId);
        await _configurationService.Delete(userGroup, cancellationToken);
        return Accepted();
    }

    /// <summary>
    /// Insert a usergroup from the configuration.
    /// </summary>
    /// <param name="userGroup">Usergroup to insert</param>
    /// <response code="201">Created</response>
    [HttpPut("usergroups", Name = "Insert user group")]
    [SwaggerResponse(201)]
    public async Task<IActionResult> Put(UserGroupConfiguration userGroup, CancellationToken cancellationToken)
    {
        _logger.LogTrace("Put(), {}", userGroup.UserGroupId);
        await _configurationService.Insert(userGroup, cancellationToken);
        return Created("todo", userGroup);
    }

    /// <summary>
    /// Enable access for a usergroup.
    /// </summary>
    /// <param name="userGroupId">Usergroup to give access to.</param>
    /// <response code="202">Accepted</response>
    [HttpPost("usergroups/enable", Name = "Enable User Group")]
    [SwaggerResponse(202)]
    public async Task<IActionResult> EnableUserGroup(string userGroupId, CancellationToken cancellationToken)
    {
        _logger.LogTrace("EnableUserGroup(), {}", userGroupId);
        await _configurationService.EnableUserGroup(userGroupId, cancellationToken);
        return Accepted();
    }

    /// <summary>
    /// Disable access for a usergroup.
    /// </summary>
    /// <param name="userGroupId">Usergroup to deny access for.</param>
    /// <response code="202">Accepted</response>
    [HttpPost("usergroups/disable", Name = "Disable User Group")]
    [SwaggerResponse(202)]
    public async Task<IActionResult> DisbleUserGroup(string userGroupId, CancellationToken cancellationToken)
    {
        _logger.LogTrace("DisbleUserGroup(), {}", userGroupId);
        await _configurationService.DisableUserGroup(userGroupId, cancellationToken);
        return Accepted();
    }

    /// <summary>
    /// Get all configured user groups.
    /// </summary>
    /// <response code="200">OK</response>
    [HttpGet("usergroups", Name = "Get User Group Configuration")]
    [SwaggerResponse(200, Type = typeof(IReadOnlyList<UserGroupConfiguration>))]
    public async Task<IActionResult> GetUserGroupConfiguration(CancellationToken cancellationToken)
    {
        _logger.LogTrace("GetUserGroupConfiguration()");
        var result = await _configurationService.GetUserGroupConfiguration(cancellationToken);
        return Ok(result);
    }

    /// <summary>
    /// Get all configured events.
    /// </summary>
    /// <response code="200">OK</response>
    [HttpGet("events", Name = "Get event Configuration")]
    [SwaggerResponse(200, Type = typeof(IReadOnlyList<EventConfiguration>))]
    public async Task<IActionResult> GetEventConfiguration(CancellationToken cancellationToken)
    {
        _logger.LogTrace("GetEventConfiguration()");
        var result = await _configurationService.GetEventConfiguration(cancellationToken);
        return Ok(result);
    }

    /// <summary>
    /// Enable receiving events for this <paramref name="eventName"/>.
    /// </summary>
    /// <param name="eventName">Receive events for this type.</param>
    /// <response code="202">Accepted</response>
    [HttpPost("events/enable", Name = "Enable Event")]
    [SwaggerResponse(202)]
    public async Task<IActionResult> EnableEvent(string eventName, CancellationToken cancellationToken)
    {
        _logger.LogTrace("EnableEvent(), {}", eventName);
        await _configurationService.EnableEvent(eventName, cancellationToken);
        return Accepted();
    }

    /// <summary>
    /// Disable receiving events for this <paramref name="eventName"/>.
    /// </summary>
    /// <param name="eventName">Disable events for this type.</param>
    /// <response code="202">Accepted</response>
    [HttpPost("events/disable", Name = "Disable Event")]
    [SwaggerResponse(202)]
    public async Task<IActionResult> DisableEvent(string eventName, CancellationToken cancellationToken)
    {
        _logger.LogTrace("DisableEvent(), {}", eventName);
        await _configurationService.DisableEvent(eventName, cancellationToken);
        return Accepted();
    }
}
