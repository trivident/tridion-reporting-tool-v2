﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using Tridion.ReportingTool.Models;
using Tridion.ReportingTool.Models.Authorization;
using Tridion.ReportingTool.Models.Contracts;

namespace Tridion.ReportingTool.Api.Controllers;

[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Policy = Policies.IsAdmin)]
[ApiController]
[Route("api/[controller]")]
public class TridionController : ControllerBase
{
    private readonly ICoreService _coreService;
    private readonly ILogger<TridionController> _logger;

    public TridionController(ICoreService coreService, ILogger<TridionController> logger)
    {
        _coreService = coreService;
        _logger = logger;
    }

    /// <summary>
    /// Trigger event system to resubscribe to all enabled events.
    /// </summary>
    /// <param name="id">Tcm uri of item to save</param>
    /// <response code="202">Accepted</response>
    [HttpPost("resubscribe", Name = "Trigger resubscribe")]
    [SwaggerResponse(202)]
    public async Task<IActionResult> Resubscribe(string id, CancellationToken cancellationToken)
    {
        _logger.LogTrace("Resubscribe(), {}", id);
        await _coreService.Resubscribe(id, cancellationToken);
        return Accepted();
    }

    /// <summary>
    /// Get all usergroups available in Tridion.
    /// </summary>
    /// <response code="200">Ok</response>
    [HttpGet("usergroups", Name = "Get all user groups")]
    [SwaggerResponse(200, Type = typeof(IReadOnlyCollection<UserGroup>))]
    public async Task<IActionResult> GetUserGroups(CancellationToken cancellationToken)
    {
        _logger.LogTrace("GetUserGroups()");
        var usergroups = await _coreService.GetUserGroups(cancellationToken);
        return Ok(usergroups);
    }
}
