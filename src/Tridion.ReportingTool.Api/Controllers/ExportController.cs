using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Net.Http.Headers;
using Tridion.ReportingTool.Models;
using Tridion.ReportingTool.Models.Authorization;
using Tridion.ReportingTool.Models.Contracts;

namespace Tridion.ReportingTool.Api.Controllers;

[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Policy = Policies.IsUser)]
[ApiController]
[Route("api/[controller]")]
public class ExportController : ControllerBase
{
    private readonly IExportService _exportService;
    private readonly ILogger<ExportController> _logger;

    public ExportController(IExportService exportService, ILogger<ExportController> logger)
    {
        _exportService = exportService;
        _logger = logger;
    }

    /// <summary>
    /// Generate an export with the given filter.
    /// </summary>
    /// <response code="200">Ok</response>
    [HttpPost(Name = "Get export")]
    [SwaggerResponse(200, Type = typeof(FileStreamResult))]
    public async Task<IActionResult> Get(EventSearchFilter filter, CancellationToken cancellationToken)
    {
        _logger.LogTrace("Get()");

        return File(await _exportService.Export(filter, cancellationToken),
            contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            fileDownloadName: $"EventsExport_{DateTime.Today:yyyyMMdd}.xlsx");
    }

    /// <summary>
    /// Generate an aggregated export with the given filter.
    /// </summary>
    /// <response code="200">Ok</response>
    [HttpPost("aggregate", Name = "Get aggregated export")]
    [SwaggerResponse(200, Type = typeof(FileStreamResult))]
    public async Task<IActionResult> GetAggregated(EventSearchFilter filter, string primaryAggregationField, string? secondaryAggregationField, CancellationToken cancellationToken)
    {
        _logger.LogTrace("GetAggregated(), {} {}", primaryAggregationField, secondaryAggregationField);
        Response.ContentType = new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet").ToString();
        return File(await _exportService.ExportAggregated(filter, primaryAggregationField, secondaryAggregationField, cancellationToken),
            contentType: new MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet").ToString(),
            fileDownloadName: $"EventsExport_{DateTime.Today:yyyyMMdd}.xlsx");
    }
}
