using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using Tridion.ReportingTool.Models;
using Tridion.ReportingTool.Models.Authorization;
using Tridion.ReportingTool.Models.Contracts;

namespace Tridion.ReportingTool.Api.Controllers;

[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Policy = Policies.IsUser)]
[ApiController]
[Route("api/[controller]")]
public class EventsController : ControllerBase
{
    private readonly IEventService _eventService;
    private readonly ILogger<EventsController> _logger;

    public EventsController(IEventService eventService, ILogger<EventsController> logger)
    {
        _eventService = eventService;
        _logger = logger;
    }

    /// <summary>
    /// Get a list of all events matching the filter criteria.
    /// </summary>
    /// <response code="200">Ok</response>
    [HttpPost(Name = "Get events")]
    [SwaggerResponse(200, Type = typeof(SearchResult<Event>))]
    public async Task<IActionResult> GetEvents(EventSearchFilter filter, int page, int pageSize, string? sortfield, SortDirection? sortdirection, CancellationToken cancellationToken)
    {
        _logger.LogTrace("Get(), {},{}", page, pageSize);
        var events = await _eventService.GetEvents(filter, page, pageSize, sortfield, sortdirection, cancellationToken);
        return Ok(events);
    }
    
    /// <summary>
    /// Get a list of all events matching the filter criteria and aggregated by the aggregation fields.
    /// </summary>
    /// <response code="200">Ok</response>
    [HttpPost("aggregate", Name = "Get aggregated events")]
    [SwaggerResponse(200, Type = typeof(SearchAggregationResult))]
    public async Task<IActionResult> GetAggregatedEvents(EventSearchFilter filter, string primaryAggregationField, string? secondaryAggregationField, CancellationToken cancellationToken)
    {
        _logger.LogTrace("GetAggregatedEvents(), {},{}", primaryAggregationField, secondaryAggregationField);
        var events = await _eventService.GetAggregatedEvents(filter, primaryAggregationField, secondaryAggregationField, cancellationToken);
        return Ok(events);
    }

    /// <summary>
    /// Get a list of auto-complete suggestions for the given field.
    /// </summary>
    /// <param name="query">Auto complete query</param>
    /// <param name="field">Field to search for</param>
    /// <response code="200">Ok</response>
    [HttpPost("suggestions", Name = "Get event suggestions")]
    [SwaggerResponse(200, Type = typeof(IReadOnlyCollection<string>))]
    public async Task<IActionResult> GetSuggestions(string query, string field, CancellationToken cancellationToken)
    {
        _logger.LogTrace("GetSuggestions(), {},{}", query, field);
        var suggestions = await _eventService.GetSuggestions(query, field, cancellationToken);
        return Ok(suggestions);
    }
    
    /// <summary>
    /// Get event by id.
    /// </summary>
    /// <param name="id">Event to retrieve</param>
    /// <response code="200">Ok</response>
    [HttpGet("{id}", Name = "Get event")]
    [SwaggerResponse(200, Type = typeof(Event))]
    public async Task<IActionResult> GetEvent(string id, CancellationToken cancellationToken)
    {
        _logger.LogTrace("Get(), {}", id);
        var @event = await _eventService.GetEvent(id, cancellationToken);
        return @event is not null ? Ok(@event) : NotFound();
    }
}
