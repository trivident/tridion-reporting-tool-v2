﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.Annotations;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Tridion.ReportingTool.Api.Options;
using Tridion.ReportingTool.Models;
using Tridion.ReportingTool.Models.Contracts;

namespace Tridion.ReportingTool.Api.Controllers;

[AllowAnonymous]
[ApiController]
[Route("api/[controller]")]
public class AuthenticationController : ControllerBase
{
    private readonly JwtOptions _jwtOptions;
    private readonly ICoreService _coreService;
    private readonly ILogger<AuthenticationController> _logger;

    public AuthenticationController(ICoreService coreService, IOptions<JwtOptions> options, ILogger<AuthenticationController> logger)
    {
        _coreService = coreService;
        _jwtOptions = options.Value;
        _logger = logger;
    }

    /// <summary>
    /// Get a bearer token
    /// </summary>
    /// <response code="200">Ok</response>
    [HttpPost(Name = "Authenticate")]
    [SwaggerResponse(200, Type = typeof(LoginResult))]
    public async Task<IActionResult> Login(LoginRequest loginRequest, CancellationToken cancellationToken)
    {
        _logger.LogTrace("Authenticate called for user {user}", loginRequest.UserName);

        if (string.IsNullOrEmpty(loginRequest.UserName))
        {
            return Ok(new LoginResult(
                IsSuccess: false,
                Token: null)
            );
        }

        var claims = new List<Claim>();

#if LOCAL
        var user = new User
        {
            Name = "UserName",
            Id = "tcm:0-1-2",
            Groups = new List<UserGroup>
                {
                    new UserGroup
                    {
                        Id = "tcm:0-1-1",
                        Name = "Everyone"
                    },
                    new UserGroup
                    {
                        Id = "tcm:0-2-1",
                        Name = "System Administrator"
                    }
                }
        };
        //User user = null;
#else
        _logger.LogDebug("Requesting groups from coreservice...");
        var user = await _coreService.GetUser(loginRequest, cancellationToken);
#endif
        if (user is null)
        {
            return Ok(new LoginResult(
                IsSuccess: false,
                Token: null)
            );
        }

        foreach (var group in user.Groups)
        {
            claims.Add(new Claim(ClaimTypes.Role, group.Name));
        }

        claims.Add(
            new Claim(ClaimTypes.Name, user.Name)
        );

        claims.Add(
            new Claim(ClaimTypes.NameIdentifier, user.Id)
        );


        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtOptions.SecurityKey));
        var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
        var expiry = DateTime.Now.AddDays(Convert.ToInt32(_jwtOptions.ExpiryInDays));

        var token = new JwtSecurityToken(
            _jwtOptions.Issuer,
            _jwtOptions.Audience,
            claims,
            expires: expiry,
            signingCredentials: creds
        );

        return Ok(new LoginResult(
            IsSuccess: true,
            Token: new JwtSecurityTokenHandler().WriteToken(token)));
    }
}
