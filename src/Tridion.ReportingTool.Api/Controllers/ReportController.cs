using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Security.Claims;
using Tridion.ReportingTool.Models;
using Tridion.ReportingTool.Models.Authorization;
using Tridion.ReportingTool.Models.Contracts;

namespace Tridion.ReportingTool.Api.Controllers;

[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Policy = Policies.IsUser)]
[ApiController]
[Route("api/[controller]")]
public class ReportController : ControllerBase
{
    private readonly IReportService _reportService;
    private readonly ILogger<ReportController> _logger;

    public ReportController(IReportService reportService, ILogger<ReportController> logger)
    {
        _reportService = reportService;
        _logger = logger;
    }

    /// <summary>
    /// Retrieve a report by id.
    /// </summary>
    /// <param name="id">Id of report</param>
    /// <response code="200">Ok</response>
    /// <response code="403">Forbidden</response>
    /// <response code="404">Not found</response>
    [HttpGet(Name = "Get report")]
    [SwaggerResponse(200, Type = typeof(Report))]
    [SwaggerResponse(403)]
    [SwaggerResponse(404)]
    public async Task<IActionResult> Get(Guid id, CancellationToken cancellationToken)
    {
        _logger.LogTrace("Get(), {}", id);
        var report = await _reportService.Get(id, cancellationToken);
        return report is not null ? Ok(report) : NotFound();
    }

    /// <summary>
    /// Get a list of auto-complete suggestions for the given field.
    /// </summary>
    /// <param name="query">Auto complete query</param>
    /// <param name="field">Field to search for</param>
    /// <response code="200">Ok</response>
    [HttpPost("suggestions", Name = "Get report suggestions")]
    [SwaggerResponse(200, Type = typeof(IReadOnlyCollection<string>))]
    public async Task<IActionResult> GetSuggestions(string query, string field, CancellationToken cancellationToken)
    {
        _logger.LogTrace("GetSuggestions(), {},{}", query, field);
        var suggestions = await _reportService.GetSuggestions(query, field, cancellationToken);
        return Ok(suggestions);
    }


    /// <summary>
    /// Delete a report by id.
    /// </summary>
    /// <param name="id">Id of report</param>
    /// <response code="200">Deleted</response>
    [HttpDelete(Name = "Delete report")]
    [SwaggerResponse(200, Type = typeof(DeleteResult))]
    public async Task<IActionResult> Delete(Guid id, CancellationToken cancellationToken)
    {
        _logger.LogTrace("Delete(), {}", id);
        var result = await _reportService.Delete(id, cancellationToken);
        return Ok(result);
    }

    /// <summary>
    /// Insert a new report.
    /// </summary>
    /// <param name="report">Report to insert</param>
    /// <response code="201">Created</response>
    [HttpPut(Name = "Insert report")]
    [SwaggerResponse(201, Type = typeof(Report))]
    public async Task<IActionResult> Put(Report report, CancellationToken cancellationToken)
    {
        _logger.LogTrace("Put(), {}", report.ReportName);
        report.ReportId = Guid.NewGuid();

        var user = HttpContext.User.FindFirstValue(ClaimTypes.Name);
        report.UserName = user;

        var result = await _reportService.Insert(report, cancellationToken);
        if (result.ResultCode == ResultCode.Success)
        {
            return Created(report.ReportId.ToString(), report);
        }
        else
        {
            return BadRequest();
        }
    }

    /// <summary>
    /// Update a report by id with new filters.
    /// </summary>
    /// <param name="id">Id of report</param>
    /// <response code="200">Updated</response>
    [HttpPatch(Name = "Update")]
    [SwaggerResponse(200, Type = typeof(UpdateResult))]
    public async Task<IActionResult> Update(Report report, CancellationToken cancellationToken)
    {
        _logger.LogTrace("Update(), {}", report.ReportId);
        var result = await _reportService.Update(report, cancellationToken);
        return Ok(result);
    }

    /// <summary>
    /// Remove the name of a user from all reports.
    /// </summary>
    /// <param name="userName">Username to remove.</param>
    [HttpPost("remove-user", Name = "Remove user")]
    [SwaggerResponse(200)]
    [SwaggerResponse(204)]
    [SwaggerResponse(403)]
    public async Task<IActionResult> RemoveUser(string userName, CancellationToken cancellationToken)
    {
        _logger.LogTrace("RemoveUser(), {}", userName);
        var result = await _reportService.RemoveUser(userName, cancellationToken);
        return result.ResultCode == ResultCode.Success ? Ok() : result.ResultCode == ResultCode.NotFound ? NoContent() : Unauthorized();
    }

    /// <summary>
    /// Get a list of all reports matching the filter criteria.
    /// </summary>
    /// <response code="200">Ok</response>
    [HttpPost(Name = "Get Reports")]
    [SwaggerResponse(200, Type = typeof(SearchResult<Report>))]
    public async Task<IActionResult> GetReports(ReportSearchFilter filter, int page, int pageSize, string? sortfield, Models.SortDirection? sortdirection, CancellationToken cancellationToken)
    {
        _logger.LogTrace("GetReports()");
        var result = await _reportService.GetReports(filter, page, pageSize, sortfield, sortdirection, cancellationToken);
        return Ok(result);
    }
}
