using Tridion.ReportingTool.Models.Configuration;

namespace Tridion.ReportingTool.Models.Contracts;

public interface IReportingToolClient
{
    /// <summary>
    /// Get bearer token
    /// </summary>
    Task<LoginResult?> Authenticate(LoginRequest loginRequest, CancellationToken cancellationToken = default);

    /// <summary>
    /// Delete a usergroup from the configuration.
    /// </summary>
    Task DeleteUserGroup(UserGroupConfiguration? body = null, CancellationToken cancellationToken = default);

    /// <summary>
    /// Insert a usergroup from the configuration.
    /// </summary>
    Task InsertUserGroup(UserGroupConfiguration? body = null, CancellationToken cancellationToken = default);

    /// <summary>
    /// Get all configured user groups.
    /// </summary>
    Task<IEnumerable<UserGroupConfiguration>?> GetUserGroupConfigurations(CancellationToken cancellationToken = default);

    /// <summary>
    /// Enable access for a usergroup.
    /// </summary>
    Task EnableUserGroup(string? userGroupId = null, CancellationToken cancellationToken = default);

    /// <summary>
    /// Disable access for a usergroup.
    /// </summary>
    Task DisableUserGroup(string? userGroupId = null, CancellationToken cancellationToken = default);

    /// <summary>
    /// Get all configured events.
    /// </summary>
    Task<IEnumerable<EventConfiguration>?> GetEventConfigurations(CancellationToken cancellationToken = default);

    /// <summary>
    /// Enable receiving events for this <paramref name="eventName"/>.
    /// </summary>
    Task EnableEvent(string? eventName = null, CancellationToken cancellationToken = default);

    /// <summary>
    /// Disable receiving events for this <paramref name="eventName"/>.
    /// </summary>
    Task DisableEvent(string? eventName = null, CancellationToken cancellationToken = default);

    /// <summary>
    /// Get a list of all events matching the filter criteria.
    /// </summary>
    Task<SearchResult<Event>?> GetEvents(int? page = null, int? pageSize = null, string? sortfield = null, SortDirection? sortdirection = null, EventSearchFilter? body = null, CancellationToken cancellationToken = default);

    /// <summary>
    /// Get a list of all events matching the filter criteria and aggregated by the aggregation fields.
    /// </summary>
    Task<SearchAggregationResult?> GetAggregatedEvents(string? primaryAggregationField = null, string? secondaryAggregationField = null, EventSearchFilter? body = null, CancellationToken cancellationToken = default);

    /// <summary>
    /// Get a list of auto-complete suggestions for the given field.
    /// </summary>
    Task<IEnumerable<string>?> GetEventSuggestions(string? query = null, string? field = null, CancellationToken cancellationToken = default);    
    
    /// <summary>
    /// Get a list of auto-complete suggestions for the given field.
    /// </summary>
    Task<IEnumerable<string>?> GetReportSuggestions(string? query = null, string? field = null, CancellationToken cancellationToken = default);

    /// <summary>
    /// Get event by id.
    /// </summary>
    Task<Event?> GetEvent(string id, CancellationToken cancellationToken = default);

    /// <summary>
    /// Generate an export with the given filter.
    /// </summary>
    Task<Stream?> GetExport(EventSearchFilter? body = null, CancellationToken cancellationToken = default);

    /// <summary>
    /// Generate an aggregated export with the given filter.
    /// </summary>
    Task<Stream?> GetAggregatedExport(string? primaryAggregationField = null, string? secondaryAggregationField = null, EventSearchFilter? body = null, CancellationToken cancellationToken = default);

    /// <summary>
    /// Retrieve a report by id.
    /// </summary>
    Task<Report?> GetReport(Guid? id = null, CancellationToken cancellationToken = default);

    /// <summary>
    /// Delete a report by id.
    /// </summary>
    Task<DeleteResult?> DeleteReport(Guid? id = null, CancellationToken cancellationToken = default);

    /// <summary>
    /// Insert a new report.
    /// </summary>
    Task<Report?> InsertReport(Report? body = null, CancellationToken cancellationToken = default);

    /// <summary>
    /// Update a report by id with new filters.
    /// </summary>
    Task UpdateReport(Report? body = null, CancellationToken cancellationToken = default);

    /// <summary>
    /// Get a list of all reports matching the filter criteria.
    /// </summary>
    Task<SearchResult<Report>?> GetReports(int? page = null, int? pageSize = null, string? sortfield = null, SortDirection? sortdirection = null, ReportSearchFilter? body = null, CancellationToken cancellationToken = default);

    /// <summary>
    /// Remove the name of a user from all reports.
    /// </summary>
    Task RemoveUser(string? userName = null, CancellationToken cancellationToken = default);

    /// <summary>
    /// Trigger event system to resubscribe to all enabled events.
    /// </summary>
    Task Resubscribe(string? id = null, CancellationToken cancellationToken = default);

    /// <summary>
    /// Get all usergroups available in Tridion.
    /// </summary>
    Task<IEnumerable<UserGroup>?> GetUserGroups(CancellationToken cancellationToken = default);
}
