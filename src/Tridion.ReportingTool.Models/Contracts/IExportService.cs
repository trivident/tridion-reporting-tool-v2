﻿namespace Tridion.ReportingTool.Models.Contracts;

public interface IExportService
{
    /// <summary>
    /// Get an excel sheet with all events matching the <paramref name="filter"/>.
    /// </summary>
    Task<Stream> Export(EventSearchFilter filter, CancellationToken cancellationToken = default);

    /// <summary>
    /// Get an excel sheet with aggregated events matching the <paramref name="filter"/>.
    /// </summary>
    Task<Stream> ExportAggregated(EventSearchFilter filter, string primaryAggregationField, string? secondaryAggregationField, CancellationToken cancellationToken = default);
}
