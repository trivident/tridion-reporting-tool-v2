﻿namespace Tridion.ReportingTool.Models.Contracts;

public interface ICoreService
{
    /// <summary>
    /// Trigger the event system to resubscribe by making a change to a secret item (default is the GIF multimedia type)
    /// </summary>
    /// <param name="id">tcm:0-1-65544</param>
    Task Resubscribe(string id = "tcm:0-1-65544", CancellationToken cancellationToken = default);

    /// <summary>
    /// Get all available user groups
    /// </summary>
    Task<IReadOnlyCollection<UserGroup>> GetUserGroups(CancellationToken cancellationToken = default);

    /// <summary>
    /// Get all user groups of a specific user
    /// </summary>
    Task<User> GetUser(LoginRequest loginRequest, CancellationToken cancellationToken = default);
}
