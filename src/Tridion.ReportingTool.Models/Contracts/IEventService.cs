﻿namespace Tridion.ReportingTool.Models.Contracts;

public interface IEventService
{
    /// <summary>
    /// Get auto complete suggestions for the given field.
    /// </summary>
    Task<IReadOnlyCollection<string>> GetSuggestions(string query, string field, CancellationToken cancellationToken = default);

    /// <summary>
    /// Get a specific event.
    /// </summary>
    Task<Event?> GetEvent(string id, CancellationToken cancellationToken = default);

    /// <summary>
    /// Get events matching the <paramref name="filter"/>.
    /// </summary>
    Task<SearchResult<Event>> GetEvents(EventSearchFilter filter, int page, int pageSize, string? sortfield = null, SortDirection? sortdirection = null, CancellationToken cancellationToken = default);

    /// <summary>
    /// Get aggregated events matching the <paramref name="filter"/>.
    /// </summary>
    Task<SearchAggregationResult> GetAggregatedEvents(EventSearchFilter filter, string primaryAggregationField, string? secondaryAggregationField = null, CancellationToken cancellationToken = default);
}
