﻿using Tridion.ReportingTool.Models.Configuration;

namespace Tridion.ReportingTool.Models.Contracts;

public interface IConfigurationService
{
    /// <summary>
    /// Disable incoming events for <paramref name="eventName"/>.
    /// </summary>
    Task DisableEvent(string eventName, CancellationToken cancellationToken = default);

    /// <summary>
    /// Enable incoming events for <paramref name="eventName"/>
    /// </summary>
    /// <param name="eventName"></param>
    /// <returns></returns>
    Task EnableEvent(string eventName, CancellationToken cancellationToken = default);

    /// <summary>
    /// Get all event configurations.
    /// </summary>
    Task<IReadOnlyList<EventConfiguration>> GetEventConfiguration(CancellationToken cancellationToken = default);

    /// <summary>
    /// Get all user group configurations.
    /// </summary>
    Task<IReadOnlyList<UserGroupConfiguration>> GetUserGroupConfiguration(CancellationToken cancellationToken = default);

    /// <summary>
    /// Insert new <paramref name="usergroup"/> into configuration.
    /// </summary>
    Task Insert(UserGroupConfiguration usergroup, CancellationToken cancellationToken = default);

    /// <summary>
    /// Delete <paramref name="usergroup"/> from configuration.
    /// </summary>
    Task Delete(UserGroupConfiguration usergroup, CancellationToken cancellationToken = default);

    /// <summary>
    /// Enable access for given <paramref name="userGroupId"/>.
    /// </summary>
    Task EnableUserGroup(string userGroupId, CancellationToken cancellationToken = default);

    /// <summary>
    /// Disable access for given <paramref name="userGroupId"/>.
    /// </summary>
    Task DisableUserGroup(string userGroupId, CancellationToken cancellationToken = default);
}
