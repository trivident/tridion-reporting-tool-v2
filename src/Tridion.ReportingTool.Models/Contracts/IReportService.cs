﻿namespace Tridion.ReportingTool.Models.Contracts;

public interface IReportService
{
    /// <summary>
    /// Get report with <paramref name="id"/>.
    /// </summary>
    Task<Report?> Get(Guid id, CancellationToken cancellationToken = default);

    /// <summary>
    /// Save new <paramref name="report"/>.
    /// </summary>
    Task<InsertResult> Insert(Report report, CancellationToken cancellationToken = default);

    /// <summary>
    /// Update <paramref name="report"/>.
    /// </summary>
    Task<UpdateResult> Update(Report report, CancellationToken cancellationToken = default);

    /// <summary>
    /// Delete report with <paramref name="id"/>.
    /// </summary>
    Task<DeleteResult> Delete(Guid id, CancellationToken cancellationToken = default);

    /// <summary>
    /// Remove all references of <paramref name="userName"/>.
    /// </summary>
    Task<DeleteResult> RemoveUser(string userName, CancellationToken cancellationToken = default);

    /// <summary>
    /// Get auto complete suggestions for the given field.
    /// </summary>
    Task<IReadOnlyCollection<string>> GetSuggestions(string query, string field, CancellationToken cancellationToken = default);

    /// <summary>
    /// Get all reports for a given
    /// </summary>
    Task<SearchResult<Report>> GetReports(ReportSearchFilter filter, int page, int pageSize, string? sortfield, SortDirection? sortdirection, CancellationToken cancellationToken = default);
}