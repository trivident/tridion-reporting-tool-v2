﻿namespace Tridion.ReportingTool.Models.Extensions;

public static class DateRangeExtensions
{
    public static DateTime FirstDayOfWeek(this DateTime dt)
    {
        var culture = Thread.CurrentThread.CurrentCulture;
        var diff = dt.DayOfWeek - culture.DateTimeFormat.FirstDayOfWeek;

        if (diff < 0)
        {
            diff += 7;
        }

        return dt.AddDays(-diff).Date;
    }

    public static DateTime LastDayOfWeek(this DateTime dt) => dt.FirstDayOfWeek().AddDays(6);

    public static DateTime FirstDayOfLastWeek(this DateTime dt) => dt.FirstDayOfWeek().AddDays(-7);

    public static DateTime LastDayOfLastWeek(this DateTime dt) => dt.FirstDayOfWeek().AddDays(-1);

    public static DateTime FirstDayOfMonth(this DateTime dt) => new(dt.Year, dt.Month, 1);

    public static DateTime LastDayOfMonth(this DateTime dt) => dt.FirstDayOfMonth().AddMonths(1).AddDays(-1);

    public static DateTime FirstDayOfLastMonth(this DateTime dt) => dt.FirstDayOfMonth().AddMonths(-1);

    public static DateTime LastDayOfLastMonth(this DateTime dt) => dt.FirstDayOfMonth().AddDays(-1);

    public static DateTime FirstDayOfYear(this DateTime dt) => new(dt.Year, 1, 1);

    public static DateTime LastDayOfYear(this DateTime dt) => dt.FirstDayOfYear().AddMonths(12).AddDays(-1);

    public static DateTime FirstDayOfLastYear(this DateTime dt) => new(dt.Year - 1, 1, 1);

    public static DateTime LastDayOfLastYear(this DateTime dt) => dt.FirstDayOfLastYear().AddMonths(12).AddDays(-1);
}