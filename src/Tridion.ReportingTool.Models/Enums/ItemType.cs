﻿namespace Tridion.ReportingTool.Models;

public enum ItemType
{
    Folder,
    StructureGroup,
    Component,
    Page,
    Keyword
}

