﻿namespace Tridion.ReportingTool.Models;

public enum PublishState
{
    None,
    WaitingForPublish,
    Rendering,
    WaitingForDeployment,
    Deploying,
    Success,
    Warning,
    Failed
}

