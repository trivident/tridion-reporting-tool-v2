﻿namespace Tridion.ReportingTool.Models;

public enum BlueprintStatus
{
    Local,
    Localized,
    Shared
}

