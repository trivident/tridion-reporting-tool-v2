﻿namespace Tridion.ReportingTool.Models;

public enum SortDirection
{
    None,
    Ascending,
    Descending
}

