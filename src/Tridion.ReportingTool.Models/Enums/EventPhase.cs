﻿namespace Tridion.ReportingTool.Models;

public enum EventPhase
{
    Initiated,
    Processed,
    TransactionCommitted,
    TransactionAborted,
    TransactionInDoubt
}
