﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tridion.ReportingTool.Models.Enums;

public enum DateOption
{
    Yesterday,
    [Display(Name = "This week")]
    Thisweek,
    [Display(Name = "Last week")]
    Lastweek,
    [Display(Name = "This month")]
    Thismonth,
    [Display(Name = "Last month")]
    Lastmonth,
    [Display(Name = "This year")]
    Thisyear,
    [Display(Name = "Last year")]
    Lastyear
}
