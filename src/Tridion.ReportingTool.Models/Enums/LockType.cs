﻿namespace Tridion.ReportingTool.Models;

public enum LockType
{
    None,
    CheckedOut,
    Permanent,
    NewItem,
    InWorkflow
}

