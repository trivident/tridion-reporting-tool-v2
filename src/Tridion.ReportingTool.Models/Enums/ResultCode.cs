﻿namespace Tridion.ReportingTool.Models;

public enum ResultCode
{
    Success,
    Failed,
    NotFound,
    UnAuthorized
}

