﻿namespace Tridion.ReportingTool.Models;

public record LoginResult(bool IsSuccess, string? Token);