﻿namespace Tridion.ReportingTool.Models;

public record ReportSearchFilter
{
    public string? Query { get; set; }
    public string? UserId { get; set; }
    public DateRange? DateRange { get; set; }
    //public bool? Shared { get; init; }
    //public bool? Predefined { get; init; }
}
