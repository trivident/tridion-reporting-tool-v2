﻿namespace Tridion.ReportingTool.Models.Authorization;

public static class Policies
{
    public const string IsAdmin = "TRTAdmin";
    public const string IsUser = "TRTUser";
}
