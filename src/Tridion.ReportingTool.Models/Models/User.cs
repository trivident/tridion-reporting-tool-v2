﻿namespace Tridion.ReportingTool.Models;

public record User
{
    public string Name { get; set; }
    public string Id { get; set; }
    public IReadOnlyCollection<UserGroup> Groups { get; set; }
}