﻿namespace Tridion.ReportingTool.Models;

public record UserGroup
{
    public string Name { get; set; }
    public string Id { get; set; }
}