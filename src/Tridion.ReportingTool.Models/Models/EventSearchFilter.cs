﻿using Tridion.ReportingTool.Models.Enums;

namespace Tridion.ReportingTool.Models;

public record EventSearchFilter
{
    public string? Query { get; set; }
    public ItemType? ItemType { get; set; }
    public string? PublicationName { get; set; }
    public string? UserName { get; set; }
    public string? ServerName { get; set; }
    public DateOption? DateOption { get; set; }
    public DateRange? DateRange { get; set; }
    public bool? Published { get; set; }
    public string? EventName { get; set; }
    public string? EventFamily { get; set; }
}
