﻿namespace Tridion.ReportingTool.Models;

public record Event
{
    public string EventId { get; set; }
    public string EventName { get; set; }
    public string EventFamily { get; set; }
    public string EventPhase { get; set; }
    public DateTime EventStartDate { get; set; }
    public string WebDavUrl { get; set; }
    public string SubjectId { get; set; }
    public string SubjectName { get; set; }
    public ItemType SubjectType { get; set; }
    public int Version { get; set; }
    public BlueprintStatus BlueprintStatus { get; set; }
    public string PublicationName { get; set; }
    public string PublicationId { get; set; }
    public string UserId { get; set; }
    public string UserName { get; set; }
    public string ServerName { get; set; }
    public int ElapsedMilliseconds { get; set; }
}
