﻿namespace Tridion.ReportingTool.Models;

public record SearchAggregationResult(IReadOnlyList<AggregationBucket> Buckets);

public record AggregationBucket
{
    public string Name { get; init; }
    public long? Total { get; set; }
    public List<AggregationResult> Results { get; set; }
}

public struct AggregationResult
{
    public string Key { get; init; }
    public long? Count { get; init; }
}