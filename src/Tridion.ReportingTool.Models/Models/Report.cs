﻿namespace Tridion.ReportingTool.Models;

public record Report
{
    public Guid? ReportId { get; set; }
    public EventSearchFilter Filter { get; set; }
    public string ReportName { get; set; }
    public DateTime Date { get; set; }
    public string? UserName { get; set; }
    public bool IsPublic { get; set; }
}
