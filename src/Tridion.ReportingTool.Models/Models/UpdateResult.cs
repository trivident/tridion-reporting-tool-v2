﻿namespace Tridion.ReportingTool.Models;

public record UpdateResult(ResultCode ResultCode);