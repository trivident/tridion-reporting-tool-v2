﻿namespace Tridion.ReportingTool.Models.Configuration;

public record UserGroupConfiguration
{
    public string UserGroupId { get; set; }
    public string UserGroupName { get; set; }
    public bool Enabled { get; set; }
}
