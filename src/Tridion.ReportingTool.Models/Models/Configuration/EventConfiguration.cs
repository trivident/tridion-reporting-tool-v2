﻿namespace Tridion.ReportingTool.Models.Configuration;

public record EventConfiguration
{
    public string EventName { get; set; }
    public bool Enabled { get; set; }
}