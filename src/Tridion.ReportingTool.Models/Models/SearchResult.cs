﻿namespace Tridion.ReportingTool.Models;

public record SearchResult<T>
{
    public SearchAggregationResult? Filters { get; set; }
    public IReadOnlyCollection<T>? Items { get; set; }
    public int? TotalItems { get; set; }
}
