﻿using Tridion.ReportingTool.Models.Enums;
using Tridion.ReportingTool.Models.Extensions;

namespace Tridion.ReportingTool.Models;

public record DateRange
{
    public DateRange()
    {

    }
    
    public DateRange(DateTime? start, DateTime? end)
    {
        Start = start;
        End = end;
    }

    public DateRange(DateOption dateOption)
    {
        CalculateDateRange(dateOption);
    }

    public DateTime? Start { get; set; }

    public DateTime? End { get; set; }

    public bool HasStartAndEndDate()
    {
        return Start != null && End != null;
    }

    private void CalculateDateRange(DateOption dateOption)
    {
        switch (dateOption)
        {
            case DateOption.Yesterday:
                Start = DateTime.Now.AddDays(-1);
                End = DateTime.Now;
                break;
            case DateOption.Thisweek:
                Start = DateRangeExtensions.FirstDayOfWeek(DateTime.Now);
                End = DateRangeExtensions.LastDayOfWeek(DateTime.Now);
                break;
            case DateOption.Lastweek:
                Start = DateRangeExtensions.FirstDayOfLastWeek(DateTime.Now);
                End = DateRangeExtensions.LastDayOfLastWeek(DateTime.Now);
                break;
            case DateOption.Thismonth:
                Start = DateRangeExtensions.FirstDayOfMonth(DateTime.Now);
                End = DateRangeExtensions.LastDayOfMonth(DateTime.Now);
                break;
            case DateOption.Lastmonth:
                Start = DateRangeExtensions.FirstDayOfLastMonth(DateTime.Now);
                End = DateRangeExtensions.LastDayOfLastMonth(DateTime.Now);
                break;
            case DateOption.Thisyear:
                Start = DateRangeExtensions.FirstDayOfYear(DateTime.Now);
                End = DateRangeExtensions.LastDayOfYear(DateTime.Now);
                break;
            case DateOption.Lastyear:
                Start = DateRangeExtensions.FirstDayOfLastYear(DateTime.Now);
                End = DateRangeExtensions.LastDayOfLastYear(DateTime.Now);
                break;
        }
    }
}
