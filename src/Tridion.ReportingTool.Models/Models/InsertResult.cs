﻿namespace Tridion.ReportingTool.Models;

public record InsertResult(ResultCode ResultCode);