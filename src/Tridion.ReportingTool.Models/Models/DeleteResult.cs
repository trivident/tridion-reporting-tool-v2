﻿namespace Tridion.ReportingTool.Models;

public record DeleteResult(ResultCode ResultCode);